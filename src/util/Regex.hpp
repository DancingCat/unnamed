#pragma once

#include <array>
#include <cstddef>
#include <type_traits>


/// Регулярные выражения
namespace regex
{
    namespace detail
    {
        template <typename T, typename Arr, typename Iter>
        inline constexpr bool invoke_match(Iter &begin, Iter end, Arr &arr)
        {
            if constexpr (T::captures == 0)
            {
                return T::match(begin, end);
            }
            else
            {
                return T::match(begin, end, arr);
            }
        }
    } // namespace detail

    /**
     * @brief Захваченная группа
     *
     * @warning С целью получения максимального быстродействия структура не владеет строкой,
     *          обращение к хранимому указателю после удаления строки, которая передавалась
     *          для проверки приведет к неопределенному поведению
     */
    template <typename Iter>
    struct Match
    {
        using type = Iter;

        Iter begin;
        Iter end;

        /**
         * @brief Преобразовать к указанному типу
         * @return Созданный объект
         *
         * @note Умолчательная реализация подразумевает std::(w)string(_view) или конструктор
         *       по двум итераторам. В случае другого объекта требуется перегрузка на стороне
         *       пользователя.
         */
        template <typename StrType>
        inline constexpr StrType to() const
        {
            return StrType{begin, end};
        }
    };

    /**
     * Список с результатом захвата последовательностей
     */
    template <typename Iter, size_t Count>
    class MatchList
    {
    public:
        constexpr MatchList() = default;

        /**
         * true если при анализе последовательность не соответствовала шаблону
         */
        constexpr bool is_empty() const
        {
            return empty;
        }

        /**
         * @brief Получить результат по инденсу
         * @param index Индекс группы
         * @return Захваченная группа символов
         *
         * @note В случае isEmpty() == true возвращает {nullptr, 0}
         */
        constexpr Match<Iter> get(size_t index) const
        {
            return data.at(index);
        }

        constexpr Match<Iter> operator[](size_t index) const
        {
            return get(index);
        }

        constexpr operator bool() const
        {
            return not is_empty();
        }

    private:
        std::array<Match<Iter>, Count> data;
        bool empty = true;

        template <typename... Ts>
        friend struct Regex;
    };

    /**
     * Регулярное выражение
     */
    template <typename... Ts>
    struct Regex
    {
        static constexpr size_t captures = (Ts::captures + ...);

        /**
         * @brief Проверить соответствие строки шаблону
         * @param begin Итератор начала последовательности
         * @param end Итератор конца последовательности
         * @return @c MatchList - список с захваченными символами - если шаблон содержит как минимум один захват
         *         @c bool - соответствует ли строка шаблону - если захватов нет
         *
         * @note Если входная строка constexpr const char * или строковый литерал,
         *       результат будет вычислен на этапе компиляции
         */
        template <typename Iter>
        static inline constexpr auto match(Iter begin, Iter end)
        {
            if constexpr (Regex<Ts...>::captures > 0)
            {
                using result_type = MatchList<Iter, Regex<Ts...>::captures>;

                result_type matches;
                if ((detail::invoke_match<Ts>(begin, end, matches.data) and ...))
                {
                    matches.empty = false;
                    return matches;
                }

                return result_type{};
            }
            else
            {
                return (Ts::match(begin, end) and ...);
            }
        }

        /**
         * @brief Проверить соответствие строки шаблону
         * @param val Последовательность для проверки
         * @return @c MatchList - список с захваченными символами - если шаблон содержит как минимум один захват
         *         @c bool - соответствует ли строка шаблону - если захватов нет
         *
         * @note Если входная строка constexpr const char * или строковый литерал,
         *       результат будет вычислен на этапе компиляции
         */
        template <typename T>
        static inline constexpr auto match(T &&val)
        {
            if constexpr (requires(T t) {{t.begin()} -> std::random_access_iterator;
                                         {t.end()} -> std::random_access_iterator; })
            {
                return match(val.begin(), val.end());
            }
            else
            {
                size_t len = 0;
                while (val[len] != 0)
                {
                    ++len;
                }
                return match(val, val + len);
            }
        }
    };

    namespace tokens
    {
        /**
         * Символ
         */
        template <char C>
        struct Char
        {
            static constexpr size_t captures = 0;

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                if (begin != end and *begin == C)
                {
                    ++begin;
                    return true;
                }

                return false;
            }
        };

        /**
         * Последовательность токенов
         */
        template <typename... Ts>
        struct Sequence
        {
            static constexpr size_t captures = (Ts::captures + ...);

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                auto source = begin;
                if (not(Ts::match(begin, end) and ...))
                {
                    begin = source;
                    return false;
                }

                return true;
            }

            template <typename Arr, typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end, Arr &matches)
            {
                static_assert(Sequence<Ts...>::captures != 0);

                auto source = begin;
                if (not(detail::invoke_match<Ts>(begin, end, matches) and ...))
                {
                    begin = source;
                    return false;
                }

                return true;
            }
        };

        /**
         * Вложенное выражение
         */
        template <typename>
        struct Expr;

        /**
         * Вложенное выражение
         */
        template <typename... Ts>
        struct Expr<Regex<Ts...>> : public Sequence<Ts...>
        {
            static constexpr size_t captures = (Ts::captures + ...);
        };

        /**
         * Вложенное выражение без захватов
         */
        template <typename>
        struct ExprNoCapture;

        /**
         * Вложенное выражение без захватов
         */
        template <typename... Ts>
        struct ExprNoCapture<Regex<Ts...>> : public Sequence<Ts...>
        {
            static constexpr size_t captures = 0;
        };

        /**
         * Последовательность символов (строка)
         */
        template <char... Cs>
        using String = Sequence<Char<Cs>...>;

        /**
         * Символ в диапазоне
         */
        template <char From, char To>
        struct Range
        {
            static constexpr size_t captures = 0;

            static_assert(From < To, "Токен 'From' должен быть меньше 'To'");

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                if (begin != end and *begin >= From and *begin <= To)
                {
                    ++begin;
                    return true;
                }

                return false;
            }
        };

        /**
         * Число
         */
        using Digit = Range<'0', '9'>;

        /// Любой токен из перечисленных
        template <typename... Ts>
        struct AnyOf
        {
            static constexpr size_t captures = (Ts::captures + ...);

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                return (Ts::match(begin, end) or ...);
            }

            template <typename Arr, typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end, Arr &matches)
            {
                static_assert(AnyOf<Ts...>::captures != 0);

                return (detail::invoke_match<Ts>(begin, end, matches) or ...);
            }
        };

        /**
         * Любой символ из перечисленных
         */
        template <char... Cs>
        using AnyOfC = AnyOf<Char<Cs>...>;

        /**
         * Пробел или табуляция
         */
        using Space = AnyOfC<' ', '\t'>;

        /**
         * Любой символ кроме перечисленных
         */
        template <char... Cs>
        struct AnyExceptC
        {
            static constexpr size_t captures = 0;

            static_assert(sizeof...(Cs) > 0, "Последовательность символов не может быть пустой");

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                if (begin != end and ((*begin == Cs) or ...))
                {
                    return false;
                }

                ++begin;
                return true;
            }
        };

        /**
         * Списка токенов может не быть в корректной последовательности
         */
        template <typename... Ts>
        struct MayBe
        {
            static constexpr size_t captures = (Ts::captures + ...);

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                [[maybe_unused]] auto _ = (Ts::match(begin, end) and ...);
                return true;
            }

            template <typename Arr, typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end, Arr &matches)
            {
                static_assert(MayBe<Ts...>::captures != 0);

                [[maybe_unused]] auto _ = (detail::invoke_match<Ts>(begin, end, matches) and ...);
                return true;
            }
        };

        /**
         * Токен встречается как минимум один раз подряд
         */
        template <typename T>
        struct OneOrMore
        {
            static constexpr size_t captures = T::captures;

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                if (not T::match(begin, end))
                {
                    return false;
                }

                while (T::match(begin, end))
                {
                }

                return true;
            }

            template <typename Arr, typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end, Arr &matches)
            {
                static_assert(OneOrMore<T>::captures != 0);

                if (not detail::invoke_match<T>(begin, end, matches))
                {
                    return false;
                }

                while (detail::invoke_match<T>(begin, end, matches))
                {
                }

                return true;
            }
        };

        /**
         * Захват значения по соответствию с токеном
         */
        template <size_t Index, typename T>
        struct Capture
        {
            static constexpr size_t captures = 1 + T::captures;

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                return T::match(begin, end);
            }

            template <typename Arr, typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end, Arr &matches)
            {
                auto source = begin;

                if constexpr (T::captures != 0)
                {
                    if (not T::match(begin, end, matches))
                    {
                        return false;
                    }
                }
                else
                {
                    if (not T::match(begin, end))
                    {
                        return false;
                    }
                }

                matches[Index] = typename Arr::value_type{.begin = source, .end = begin};

                return true;
            }
        };

        /**
         * Игнорировать любой токен (конец последовательности не важен)
         */
        struct Ignore
        {
            static constexpr size_t captures = 0;

            template <typename Iter>
            static inline constexpr bool match(Iter &begin, Iter end)
            {
                begin = end;
                return true;
            }
        };
    } // namespace tokens
} // namespace regex

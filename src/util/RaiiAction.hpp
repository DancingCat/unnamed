#pragma once


/**
 * RAII-объект с действием в деструкторе
 */
template <typename Func>
class RaiiAction
{
    Func func;

public:
    RaiiAction(Func &&func)
        : func(func)
    {
    }

    ~RaiiAction()
    {
        func();
    }

    RaiiAction(RaiiAction &) = delete;
    RaiiAction(RaiiAction &&) = delete;
    RaiiAction &operator=(RaiiAction &) = delete;
    RaiiAction &operator=(RaiiAction &&) = delete;
};

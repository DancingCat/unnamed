#pragma once

#include <string>


/**
 * Базовый класс исключений
 */
class Exception
{
public:
    /**
     * @brief Базовый класс исключений
     * @param what Причина исключения
     */
    Exception(std::string what)
        : str(std::move(what))
    {
    }

    Exception(std::string_view what)
        : str{what}
    {
    }

    /**
     * Получить причину исключения
     */
    const std::string &what() const
    {
        return str;
    }

private:
    std::string str;
};

/**
 * Ошибка сериализации
 */
class SerializationError : public Exception
{
public:
    using Exception::Exception;
};

/**
 * Запрашиваемое значение не найдено
 */
class NoSuchValue : public Exception
{
public:
    using Exception::Exception;
};

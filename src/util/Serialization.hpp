#pragma once

#include <array>
#include <charconv>
#include <regex>
#include <sstream>
#include <string_view>

#include "Exception.hpp"


/**
 * @interface Serializable
 * Обобщенный интерфейс для сериализации объектов.
 *
 * Для работы требуется объект, имеющий определенный интерфейс.
 *
 * Для получения объекта из строки:
 * 1. Имеется статический метод Object Object::from_string(std::string)
 * 2. Имеется конструктор Object::Object(std::string)
 * 3. Перегружен оператор вывода в поток, подразумевается текстовый поток
 *
 * Для конвертации объекта в строку:
 * 1. Метод Object::to_string() const (не статический)
 * 2. Перегружен оператор приведения в std::string
 * 3. Перегружен оператор чтения из потока std::istream, подразумевается текстовый поток
 * 4. Доступно использование функции std::to_string
 *
 * Для использования интерфейса необходимо соблюдение хотябы одного из условий для ввода и вывода. Если
 * доступно несколько спобосов ввода/вывода, будет выбран тот, который расположен первым по списку выше.
 *
 * При работе со строкой конвертация не производится, будет возвращено переданное значение.
 */


namespace detail
{
    template <typename T>
    concept FromStringConstructible = requires(std::string_view str) {
        T{str};
    };

    template <typename T>
    concept FromStringStatic = requires(std::string_view str) {
        {
            T::from_string(str)
        } -> std::convertible_to<T>;
    };

    template <typename T>
    concept FromStringStream = requires(std::istream in, T t) {
        T{};
        in >> t;
    };

    template <typename T>
    concept ToStringConvertible = std::is_convertible_v<T, std::string>;

    template <typename T>
    concept ToStringStatic = requires(const T t) {
        {
            t.to_string()
        } -> std::convertible_to<std::string>;
    };

    template <typename T>
    concept ToStringStream = requires(std::ostream out, T t) {
        out << t;
    };
} // namespace detail

/**
 * Обобщенные функции конвертации объекта
 *
 * @interface Serializable
 */
template <typename T>
struct Convert
{
    /**
     * Конвертировать строку в объект
     */
    static T from_string(std::string_view str)
    {
        if constexpr (std::is_same_v<T, std::string>)
        {
            return std::string{str};
        }

        if constexpr (detail::FromStringStatic<T>)
        {
            return T::from_string(str);
        }
        else if constexpr (detail::FromStringConstructible<T>)
        {
            return T{str};
        }
        else if constexpr (detail::FromStringStream<T>)
        {
            std::istringstream in{std::string{str}};
            T obj{};
            in >> obj;
            return obj;
        }
    }

    /**
     * Конвертировать объект в строку
     */
    static std::string to_string(const T &obj)
    {
        if constexpr (std::is_same_v<T, std::string> or std::is_same_v<T, std::string_view>)
        {
            return std::string{obj};
        }

        if constexpr (detail::ToStringStatic<T>)
        {
            return obj.to_string();
        }
        else if constexpr (detail::ToStringConvertible<T>)
        {
            return static_cast<std::string>(obj);
        }
        else if constexpr (detail::ToStringStream<T>)
        {
            std::ostringstream out;
            out << obj;
            return out.str();
        }
        else
        {
            return std::to_string(obj);
        }
    }
};

/**
 * Обобщенные функции конвертации чисел
 *
 * @interface Serializable
 */
template <typename T>
    requires std::integral<T> or std::floating_point<T>
struct Convert<T>
{
    /**
     * Конвертировать строку в число
     */
    static T from_string(std::string_view str)
    {
        T result;
        if (auto &&[_, err] = std::from_chars(str.data(), str.data() + str.size(), result); err != std::errc{})
        {
            throw SerializationError{std::string{str}};
        }

        return result;
    }

    /**
     * Конвертировать число в строку
     */
    static std::string to_string(T val)
    {
        return std::to_string(val);
    }
};

/**
 * Обобщенные функции конвертации bool
 *
 * @interface Serializable
 */
template <>
struct Convert<bool>
{
    static constexpr const char *true_str = "true";
    static constexpr const char *false_str = "false";

    /**
     * Конвертировать строку в bool
     */
    static bool from_string(std::string_view str)
    {
        if (str == true_str)
        {
            return true;
        }

        if (str == false_str)
        {
            return false;
        }

        throw SerializationError{std::string{str}};
    }

    /**
     * Конвертировать bool в строку
     */
    static std::string to_string(bool val)
    {
        return val ? true_str : false_str;
    }
};

/**
 * Обобщенные функции конвертации строк для записи
 *
 * При конвертации происходит замена спец. символов
 *
 * @interface Serializable
 */
template <>
struct Convert<std::string>
{
    static constexpr auto table = std::array{
        std::pair{'"',  "&quote;"    },
        std::pair{'\n', "&newline;"  },
        std::pair{'&',  "&ampersand;"}
    };

    static auto find_in_table(char c)
    {
        return std::find_if(table.begin(), table.end(), [c](auto &&p) { return p.first == c; });
    }

    static auto find_in_table(std::string_view str)
    {
        return std::find_if(table.begin(), table.end(), [str](auto &&p) { return p.second == str; });
    }

    static std::string convert_to_sequence(char c)
    {
        auto iter = find_in_table(c);
        return iter == table.end()
                 ? std::string{} + c
                 : iter->second;
    }

    /**
     * Получить строку из записанной
     */
    static std::string from_string(std::string_view str)
    {
        static std::array<std::pair<std::regex, std::string>, table.size()> regexes;
        [[maybe_unused]] static auto _ = std::invoke([] {
            for (size_t i = 0; i < table.size(); ++i)
            {
                auto &&[c, str] = table[i];
                regexes[i] = {std::regex{str}, std::string{} + c};
            }
            return 0;
        });

        std::string result = {str.begin(), str.end()};
        for (auto &&[re, str] : regexes)
        {
            result = std::regex_replace(result, re, str);
        }
        return result;
    }

    /**
     * Конвертировать в строку, пригодную для записи
     */
    static std::string to_string(std::string_view str)
    {
        std::string result;
        for (char c : str)
        {
            result += convert_to_sequence(c);
        }
        return result;
    }
};

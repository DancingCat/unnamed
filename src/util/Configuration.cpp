#include "Configuration.hpp"

#include <fstream>

#include "util/Regex.hpp"


static bool load_pairs(std::map<std::string, std::string> &dst, std::ifstream &in)
{
    using namespace regex::tokens;

    using TrailingSpaces = MayBe<OneOrMore<Space>>;

    using Comment = regex::Regex<TrailingSpaces,
                                 Char<'#'>,
                                 Space,
                                 Ignore>;

    using EmptyLine = regex::Regex<OneOrMore<Space>>;

    using Pair = regex::Regex<TrailingSpaces,
                              Char<'"'>,
                              Capture<0,
                                      OneOrMore<AnyExceptC<'"'>>>,
                              TrailingSpaces,
                              Char<'"'>,
                              TrailingSpaces,
                              Char<':'>,
                              TrailingSpaces,
                              Char<'"'>,
                              Capture<1,
                                      OneOrMore<AnyExceptC<'"'>>>,
                              Char<'"'>,
                              Ignore>;

    std::string str;
    bool errors = false;

    while (std::getline(in, str))
    {
        if (str.empty() or Comment::match(str) or EmptyLine::match(str))
        {
            continue;
        }

        auto match = Pair::match(str);
        if (not match)
        {
            errors = true;
            continue;
        }

        dst[match[0].to<std::string>()] = match[1].to<std::string>();
    }

    return errors;
}

Configuration::Configuration(const std::filesystem::path &file)
    : file{file}
{
    reload();
}

Configuration::~Configuration()
{
    if (not need_save or data.empty())
    {
        return;
    }

    save();
}

void Configuration::reload()
{
    if (not std::filesystem::exists(file))
    {
        return;
    }

    std::ifstream in{file};
    data.clear();
    errors_on_load = false;

    using namespace regex::tokens;

    using TrailingSpaces = MayBe<OneOrMore<Space>>;

    using Comment = regex::Regex<TrailingSpaces,
                                 Char<'#'>,
                                 Space,
                                 Ignore>;

    using EmptyLine = regex::Regex<OneOrMore<Space>>;

    using Pair = regex::Regex<TrailingSpaces,
                              Char<'"'>,
                              Capture<0,
                                      OneOrMore<AnyExceptC<'"'>>>,
                              TrailingSpaces,
                              Char<'"'>,
                              TrailingSpaces,
                              Char<':'>,
                              TrailingSpaces,
                              Char<'"'>,
                              Capture<1,
                                      OneOrMore<AnyExceptC<'"'>>>,
                              Char<'"'>,
                              Ignore>;

    std::string str;

    while (std::getline(in, str))
    {
        if (str.empty() or Comment::match(str) or EmptyLine::match(str))
        {
            continue;
        }

        auto match = Pair::match(str);
        if (not match)
        {
            errors_on_load = true;
            continue;
        }

        data[match[0].to<std::string>()] = match[1].to<std::string>();
    }
}

void Configuration::save()
{
    std::ofstream out{file};

    for (auto &&[name, value] : data)
    {
        out << std::quoted(name) << ": " << std::quoted(value) << std::endl;
    }
}

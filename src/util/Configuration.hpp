#pragma once

#include <filesystem>
#include <iostream>
#include <map>
#include <string>

#include "Serialization.hpp"


/**
 * Конфигурация приложения с сохранением в файл
 */
class Configuration
{
    std::filesystem::path file;
    bool need_save = true;
    bool errors_on_load = false;
    std::map<std::string, std::string> data;

public:
    /**
     * Конфигурация приложения с сохранением в файл
     * @param file Файл для загрузки/сохранения
     */
    Configuration(const std::filesystem::path &file);

    ~Configuration();

    Configuration(const Configuration &) = delete;
    Configuration(const Configuration &&) = delete;
    Configuration &operator=(const Configuration &) = delete;
    Configuration &operator=(const Configuration &&) = delete;

    /**
     * Перезагрузить данные из файла
     *
     * @note Текущие данные будут стерты
     */
    void reload();

    /**
     * Записать данные в файл
     *
     * @note Игнорирует флаг need_save
     */
    void save();

    /**
     * Наличие ошибок при загрузке данных из файла
     */
    inline bool has_errors_on_load() const
    {
        return errors_on_load;
    }

    /**
     * Установить необходимость сохранения (в деструкторе)
     */
    inline void set_need_save(bool value)
    {
        need_save = value;
    }

    template <typename T>
    void set_value(const std::string &name, T &&value)
    {
        data[name] = Convert<T>::to_string(value);
    }

    /**
     * Получить значение по имени
     * @tparam T Тип запрашиваемого значения
     * @param name Имя параметра
     * @param def_val Умолчательное значение (в случае ошибки)
     * @return Значение параметра или умолчательное
     */
    template <typename T>
    T get_value(const std::string &name, const T &def_val) const noexcept
    {
        auto iter = data.find(name);
        if (iter == data.end())
        {
            return def_val;
        }

        try
        {
            return Convert<T>::from_string(iter->second);
        }
        catch (const SerializationError &err)
        {
            return def_val;
        }
    }

    /**
     * Получить значение по имени
     * @tparam T Тип запрашиваемого значения
     * @param name Имя параметра
     * @return Значение параметра
     *
     * @throw NoSuchValue если значение отсутствует
     * @throw SerializationError В случае ошибки приведения к нужному типу
     */
    template <typename T>
    T get_value(const std::string &name) const
    {
        auto iter = data.find(name);
        if (iter == data.end())
        {
            throw NoSuchValue(name);
        }

        try
        {
            return Convert<T>::from_string(iter->second);
        }
        catch (const SerializationError &err)
        {
            throw SerializationError(iter->second);
        }
    }
};

#include "Application.hpp"

#include <cassert>
#include <chrono>
#include <iostream>
#include <thread>

#include <SDL.h>

#include "game/GameWidget.hpp"
#include "ui/MainMenu.hpp"
#include "ui/Window.hpp"


namespace
{
    MouseButtonEvent convert(const SDL_MouseButtonEvent &event)
    {
        MouseButton button;
        switch (event.button)
        {
        case SDL_BUTTON_LEFT:
            button = MouseButton::Left;
            break;

        case SDL_BUTTON_RIGHT:
            button = MouseButton::Right;
            break;

        default:
            button = MouseButton::Undefined;
            break;
        }

        return {.x = event.x, .y = event.y, .button = button, .pressed = (event.state == SDL_PRESSED)};
    }

    MouseMoveEvent convert(const SDL_MouseMotionEvent &event)
    {
        return {.x = event.x, .y = event.y};
    }

    KeyPressEvent convert(const SDL_KeyboardEvent &event)
    {
        return {.key = event.keysym.scancode, .pressed = (event.state == SDL_PRESSED)};
    }
} // namespace


Application *Application::instance = nullptr;


Application &Application::get_instance()
{
    assert(instance != nullptr);
    return *instance;
}


Application::Application()
{
    assert(instance == nullptr);

    instance = this;
    window_widget = new Window("unnamed", Vector2D(1920, 1080));

    auto main_menu = new MainMenu(window_widget->get_size());
    window_widget->add_child(main_menu);

    auto game_widget = new GameWidget(window_widget->get_size());
    window_widget->add_child(game_widget);
    window_widget->set_keyboard_focus(game_widget);

    game_widget->set_visible(false);

    main_menu->on_start_clicked = [game_widget, main_menu]() {
        game_widget->set_visible(true);
        main_menu->set_visible(false);
    };
    main_menu->on_options_clicked = []() { std::cout << "options" << std::endl; };
    main_menu->on_exit_clicked = []() { Application::get_instance().quit(); };
}


Application::~Application()
{
    instance = nullptr;

    delete window_widget;
}


int Application::execute()
{
    using namespace std::chrono;

    assert(window_widget != nullptr);

    if (code != 0)
    {
        return code;
    }

    if (run)
    {
        return -1;
    }

    run = true;

    const auto frame_time = duration_cast<microseconds>(seconds(1)) / 60; // max fps
    SDL_Event event;
    steady_clock::time_point timestamp;

    while (run)
    {
        timestamp = std::chrono::steady_clock::now();

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit();
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                if (event.key.repeat == 0)
                {
                    KeyPressEvent kp_event = convert(event.key);
                    window_widget->dispatch_event(kp_event);
                }
                break;

            case SDL_MOUSEMOTION:
            {
                MouseMoveEvent mm_event = convert(event.motion);
                window_widget->dispatch_event(mm_event);
            }
            break;

            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            {
                MouseButtonEvent mb_event = convert(event.button);
                window_widget->dispatch_event(mb_event);
            }
            break;

            default:
                break;
            }
        }

        auto delta = timestamp - steady_clock::now();
        if (delta < frame_time)
        {
            std::this_thread::sleep_for(frame_time - delta);
            delta = frame_time;
        }

        TickEvent t_event{.time_delta = static_cast<double>(duration_cast<microseconds>(delta).count()) / 1000000.0f};
        window_widget->dispatch_event(t_event);

        SDL_Renderer *renderer = window_widget->get_renderer();

        SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
        SDL_RenderClear(renderer);

        window_widget->dispatch_event(PaintEvent{});

        SDL_RenderPresent(renderer);
    }

    return code;
}


void Application::quit(int exitCode)
{
    code = exitCode;
    run = false;
}

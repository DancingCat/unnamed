#include "MathCollision.hpp"


bool collision::point_rect(Point2D point, const Rect2D &rect)
{
    return point.x >= rect.get_left_top().x
       and point.y >= rect.get_left_top().y
       and point.x <= rect.get_left_top().x + rect.get_size().dx
       and point.y <= rect.get_left_top().y + rect.get_size().dy;
}


bool collision::rect_rect(const Rect2D &first, const Rect2D &second)
{
    return true;
}


bool collision::rect_circle(const Rect2D &rect, Point2D pos, double radius)
{
    double x = pos.x;
    double y = pos.y;

    if (pos.x < rect.get_left_top().x)
    {
        x = rect.get_left_top().x;
    }
    else if (pos.x > rect.get_left_top().x + rect.get_size().dx)
    {
        x = rect.get_left_top().x + rect.get_size().dx;
    }

    if (pos.y < rect.get_left_top().y)
    {
        y = rect.get_left_top().y;
    }
    else if (pos.y > rect.get_left_top().y + rect.get_size().dy)
    {
        y = rect.get_left_top().y + rect.get_size().dy;
    }

    return std::sqrt(std::pow(pos.x - x, 2) + std::pow(pos.y - y, 2)) <= radius;
}

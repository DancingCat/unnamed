#pragma once

#include "math/Point2D.hpp"
#include "math/Vector2D.hpp"


/**
 * Отрезок
 */
class Line2D
{
    Point2D first;
    Point2D second;

public:
    /// Создать с нулевыми размерами
    static inline Line2D null()
    {
        return Line2D{Point2D::null(), Point2D::null()};
    }

    /**
     * @brief Отрезок
     * @param first Начало отрезка
     * @param second Конец отрезка
     */
    Line2D(Point2D first, Point2D second)
        : first(first)
        , second(second)
    {
    }

    /// Длина
    inline double get_length() const
    {
        return get_direction().get_length();
    }

    /// Направление
    inline Vector2D get_direction() const
    {
        return {second.x - first.x, second.y - first.y};
    }

    /// Начало отрезка
    inline Point2D get_first_point() const
    {
        return first;
    }

    /// Установить начало отрезка
    inline void set_first_point(Point2D first)
    {
        this->first = first;
    }

    /// Конец отрезка
    inline Point2D get_second_point() const
    {
        return second;
    }

    /// Установить конец отрезка
    inline void set_second_point(Point2D second)
    {
        this->second = second;
    }

    /**
     * @brief Прочитать из строки
     * @param str Строка
     * @return Созданный объект
     *
     * @interface Serializable
     * @throw SerializationError в случае ошибки
     */
    static Line2D from_string(std::string_view str) noexcept(false);

    /**
     * @brief Преобразовать с строку
     * @return Строка
     *
     * @interface Serializable
     */
    std::string to_string() const;
};

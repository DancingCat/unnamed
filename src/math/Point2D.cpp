#include "Point2D.hpp"

#include <string>

#include "math/MathRegex.hpp"
#include "util/Exception.hpp"
#include "util/Serialization.hpp"


Point2D Point2D::from_string(std::string_view str) noexcept(false)
{
    auto match = math_regex::Point2D<0, 1>::match(str);
    if (not match)
    {
        throw SerializationError(str);
    }

    return Point2D{Convert<double>::from_string(match[0].to<std::string>()),
                   Convert<double>::from_string(match[1].to<std::string>())};
}

std::string Point2D::to_string() const
{
    // format:
    // @Point2D{ 3.14, -5 }
    return "@Point2D{ " + Convert<double>::to_string(x) + ", " + Convert<double>::to_string(y) + " }";
}

#pragma once

#include "math/Point2D.hpp"
#include "math/Vector2D.hpp"


/**
 * Прямоугольник
 */
class Rect2D
{
    Point2D left_top;
    Vector2D size;

public:
    /**
     * @brief Вычислить центр прямоугольника по точке и размеру
     * @param pos Левый верхний угол прямоугольника
     * @param size Размер прямоугольника
     * @return Центр прямоугольника
     */
    static inline Point2D center_of(Point2D pos, Vector2D size)
    {
        return Point2D(pos.x + size.dx / 2, pos.y + size.dy / 2);
    }

    /**
     * Создать с нулевыми размерами
     */
    static inline Rect2D null()
    {
        return Rect2D{Point2D::null(), Vector2D::null()};
    }

    /**
     * @brief Прямоугольник
     * @param leftTop Верхний левый угол
     * @param size Размер
     *
     * @note Размер устанавливается по абсолютной величине, игнорируя направление
     */
    Rect2D(Point2D leftTop, Vector2D size)
        : left_top(leftTop)
        , size(size.get_absolute_value())
    {
    }

    /**
     * @brief Прямоугольник по точке в центре
     * @param center Расположение центра
     * @param size Размер
     *
     * @note Размер устанавливается по абсолютной величине, игнорируя направление
     */
    inline static Rect2D create_by_center(Point2D center, Vector2D size)
    {
        return {center - size.get_absolute_value() / 2, size};
    }

    /**
     * @brief Прямоугольник по двум точкам
     * @param first Первая точка
     * @param second Вторая точка
     *
     * @note Порядок точек не имеет значения, выбираются по min/max
     */
    inline static Rect2D create_by_points(Point2D first, Point2D second)
    {
        return {Point2D(std::min(first.x, second.x),
                        std::min(first.y, second.y)),
                Vector2D::from_points(first, second)};
    }

    /**
     * Положение левого верхнего угла
     */
    inline Point2D get_left_top() const
    {
        return left_top;
    }

    /**
     * Установить положение левого верхнего угла
     */
    inline void set_left_top(Point2D value)
    {
        left_top = value;
    }

    /**
     * Размер (значения всегда положительные)
     */
    inline Vector2D get_size() const
    {
        return size;
    }

    /**
     * @brief Задать размер
     * @param value Значение размера
     *
     * @note Размер устанавливается по абсолютной величине, игнорируя направление
     */
    inline void set_size(Vector2D value)
    {
        size = value.get_absolute_value();
    }

    /**
     * Передвинуть на значение delta
     */
    inline void move(Vector2D delta)
    {
        left_top += delta;
    }

    /**
     * @brief Прочитать из строки
     * @param str Строка
     * @return Созданный объект
     *
     * @interface Serializable
     * @throw SerializationError в случае ошибки
     */
    static Rect2D from_string(std::string_view str) noexcept(false);

    /**
     * @brief Преобразовать с строку
     * @return Строка
     *
     * @interface Serializable
     */
    std::string to_string() const;
};

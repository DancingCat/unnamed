#include "Vector2D.hpp"

#include "math/MathRegex.hpp"
#include "util/Exception.hpp"
#include "util/Serialization.hpp"


Vector2D Vector2D::from_string(std::string_view str) noexcept(false)
{
    auto match = math_regex::Vector2D<0, 1>::match(str);
    if (not match)
    {
        throw SerializationError(str);
    }

    return Vector2D{Convert<double>::from_string(match[0].to<std::string>()),
                    Convert<double>::from_string(match[1].to<std::string>())};
}

std::string Vector2D::to_string() const
{
    // format:
    // @Vector2D{ 3.14, -5 }
    return "@Vector2D{ " + Convert<double>::to_string(dx) + ", " + Convert<double>::to_string(dy) + " }";
}

#pragma once

#include "math/Rect2D.hpp"


/// Проверка коллизий
namespace collision
{
    /**
     * @brief Нахождение точки внутри прямоугольника
     */
    bool point_rect(Point2D point, const Rect2D &rect);

    /**
     * @brief Нахождение прямоугольника внутри прямоугольника или касание
     */
    bool rect_rect(const Rect2D &first, const Rect2D &second);

    /**
     * @brief Коллизия прямоугольника с кругом
     * @param rect Прямоугольник
     * @param pos Центр круга
     * @param radius Радиус круга
     * @return true в случае коллизии
     */
    bool rect_circle(const Rect2D &rect, Point2D pos, double radius);
} // namespace collision

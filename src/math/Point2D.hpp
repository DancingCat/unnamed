#pragma once

#include <cmath>
#include <string>

#include "math/Math.hpp"


/**
 * Точка
 */
struct Point2D
{
    double x;
    double y;

    /**
     * Создать с нулевыми размерами
     */
    static inline Point2D null()
    {
        return Point2D{0, 0};
    }

    /**
     * @brief Эквивалентность двух точек
     */
    static inline bool equals(Point2D first, Point2D second)
    {
        return ::is_floats_equal(first.x, second.x) and ::is_floats_equal(first.y, second.y);
    }

    /**
     * Точка
     */
    inline Point2D(double x, double y)
        : x(x)
        , y(y)
    {
    }

    /**
     * @brief Округлить в нижнюю сторону
     */
    inline Point2D trunc() const
    {
        return Point2D(std::trunc(x), std::trunc(y));
    }

    /**
     * @brief Округлить в верхнюю сторону
     */
    inline Point2D ceil() const
    {
        return Point2D(std::ceil(x), std::ceil(y));
    }

    /**
     * @brief Прочитать из строки
     * @param str Строка
     * @return Созданный объект
     *
     * @interface Serializable
     * @throw SerializationError в случае ошибки
     */
    static Point2D from_string(std::string_view str) noexcept(false);

    /**
     * @brief Преобразовать с строку
     * @return Строка
     *
     * @interface Serializable
     */
    std::string to_string() const;
};

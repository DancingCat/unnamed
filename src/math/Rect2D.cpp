#include "Rect2D.hpp"

#include "math/MathRegex.hpp"
#include "util/Exception.hpp"
#include "util/Serialization.hpp"


Rect2D Rect2D::from_string(std::string_view str) noexcept(false)
{
    auto match = math_regex::Rect2D<0, 1>::match(str);
    if (not match)
    {
        throw SerializationError{str};
    }

    return Rect2D{Convert<Point2D>::from_string(match[0].to<std::string_view>()),
                  Convert<Vector2D>::from_string(match[1].to<std::string_view>())};
}

std::string Rect2D::to_string() const
{
    // format:
    // @Rect2D{ @Point2D{ 3.14, -5 }, @Vector2D{ 3.14, -5 } }
    return "@Rect2D{ " + Convert<Point2D>::to_string(left_top) + ", " + Convert<Vector2D>::to_string(size) + " }";
}

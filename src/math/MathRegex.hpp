#pragma once

#include "util/Regex.hpp"


namespace math_regex
{
    using namespace regex::tokens;

    using Number = regex::Regex<MayBe<AnyOfC<'-', '+'>>,
                                OneOrMore<Digit>,
                                MayBe<Char<'.'>,
                                      OneOrMore<Digit>>>;

    using TrailingSpaces = MayBe<OneOrMore<Space>>;

    template <size_t First, size_t Second>
    using DigitPair = regex::Regex<Char<'{'>,
                                   TrailingSpaces,
                                   Capture<First,
                                           Expr<Number>>,
                                   TrailingSpaces,
                                   Char<','>,
                                   TrailingSpaces,
                                   Capture<Second,
                                           Expr<Number>>,
                                   TrailingSpaces,
                                   Char<'}'>>;

    template <size_t X, size_t Y>
    using Point2D = regex::Regex<String<'@', 'P', 'o', 'i', 'n', 't', '2', 'D'>,
                                 TrailingSpaces,
                                 Expr<DigitPair<X, Y>>>;

    template <size_t DX, size_t DY>
    using Vector2D = regex::Regex<String<'@', 'V', 'e', 'c', 't', 'o', 'r', '2', 'D'>,
                                  TrailingSpaces,
                                  Expr<DigitPair<DX, DY>>>;

    static inline constexpr size_t no_index = -1;

    template <size_t P1, size_t P2>
    using Line2D = regex::Regex<String<'@', 'L', 'i', 'n', 'e', '2', 'D'>,
                                TrailingSpaces,
                                Char<'{'>,
                                TrailingSpaces,
                                Capture<P1,
                                        ExprNoCapture<Point2D<no_index, no_index>>>,
                                TrailingSpaces,
                                Char<','>,
                                TrailingSpaces,
                                Capture<P2,
                                        ExprNoCapture<Point2D<no_index, no_index>>>,
                                TrailingSpaces,
                                Char<'}'>>;

    template <size_t Pos, size_t Size>
    using Rect2D = regex::Regex<String<'@', 'R', 'e', 'c', 't', '2', 'D'>,
                                TrailingSpaces,
                                Char<'{'>,
                                TrailingSpaces,
                                Capture<Pos,
                                        ExprNoCapture<Point2D<no_index, no_index>>>,
                                TrailingSpaces,
                                Char<','>,
                                TrailingSpaces,
                                Capture<Size,
                                        ExprNoCapture<Vector2D<no_index, no_index>>>,
                                TrailingSpaces,
                                Char<'}'>>;
} // namespace math_regex

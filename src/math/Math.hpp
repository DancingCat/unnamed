#pragma once

#include <cmath>
#include <limits>


/**
 * Равенство чисел с плавающей точкой
 */
inline bool is_floats_equal(double f1, double f2)
{
    return std::abs(f1 - f2) < std::numeric_limits<double>().epsilon();
}

/**
 * Равенство числа с плавающей точкой нулю
 */
inline bool is_float_zero(double f)
{
    return std::abs(f) < std::numeric_limits<double>().epsilon();
}

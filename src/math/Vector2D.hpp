#pragma once

#include <cmath>
#include <string>

#include "math/Math.hpp"
#include "math/Point2D.hpp"


/**
 * Векторная величина
 */
struct Vector2D
{
    double dx;
    double dy;

    /**
     * Создать с нулевыми размерами
     */
    static inline Vector2D null()
    {
        return Vector2D{0, 0};
    }

    /**
     * Векторная величина по двум точкам
     */
    inline static Vector2D from_points(Point2D p1, Point2D p2)
    {
        return {p2.x - p1.x, p2.y - p1.y};
    }

    /**
     * Векторная величина
     */
    inline Vector2D(double dx, double dy)
        : dx(dx)
        , dy(dy)
    {
    }

    /**
     * @brief Округлить в нижнюю сторону
     */
    inline Vector2D trunc() const
    {
        return Vector2D(std::trunc(dx), std::trunc(dy));
    }

    /**
     * @brief Округлить в верхнюю сторону
     */
    inline Vector2D ceil() const
    {
        return Vector2D(std::ceil(dx), std::ceil(dy));
    }

    /**
     * Скалярное значение
     */
    inline double get_length() const
    {
        return std::sqrt(dx * dx + dy * dy);
    }

    /**
     * Конвертировать в единичный вектор
     */
    inline Vector2D normalized() const
    {
        double len = get_length();
        return {dx / len, dy / len};
    }

    /**
     * Преобразовать значения к абсолютным
     */
    inline Vector2D get_absolute_value() const
    {
        return {std::abs(dx), std::abs(dy)};
    }

    /**
     * @brief Прочитать из строки
     * @param str Строка
     * @return Созданный объект
     *
     * @interface Serializable
     * @throw SerializationError в случае ошибки
     */
    static Vector2D from_string(std::string_view str) noexcept(false);

    /**
     * @brief Преобразовать с строку
     * @return Строка
     *
     * @interface Serializable
     */
    std::string to_string() const;
};

inline Point2D operator+(Point2D p, Vector2D v)
{
    return {p.x + v.dx, p.y + v.dy};
}

inline Point2D &operator+=(Point2D &p, Vector2D v)
{
    p = p + v;
    return p;
}

inline Point2D operator-(Point2D p, Vector2D v)
{
    return {p.x - v.dx, p.y - v.dy};
}

inline Point2D &operator-=(Point2D &p, Vector2D v)
{
    p = p - v;
    return p;
}

inline Vector2D operator+(Vector2D v1, Vector2D v2)
{
    return {v1.dx + v2.dx, v1.dy + v2.dy};
}

inline Vector2D &operator+=(Vector2D &v1, Vector2D v2)
{
    v1 = v1 + v2;
    return v1;
}

inline Vector2D operator-(Vector2D v1, Vector2D v2)
{
    return {v1.dx - v2.dx, v1.dy - v2.dy};
}

inline Vector2D &operator-=(Vector2D &v1, Vector2D v2)
{
    v1 = v1 - v2;
    return v1;
}

inline Vector2D operator*(Vector2D v, double f)
{
    return {v.dx * f, v.dy * f};
}

inline Vector2D &operator*=(Vector2D &v, double f)
{
    v = v * f;
    return v;
}

inline Vector2D operator/(Vector2D v, double f)
{
    return {v.dx / f, v.dy / f};
}

inline Vector2D &operator/=(Vector2D &v, double f)
{
    v = v / f;
    return v;
}

inline Vector2D operator-(Vector2D v)
{
    return {-v.dx, -v.dy};
}

inline bool operator==(Vector2D v1, Vector2D v2)
{
    return is_floats_equal(v1.dx, v2.dx) and is_floats_equal(v1.dy, v2.dy);
}

inline bool operator!=(Vector2D v1, Vector2D v2)
{
    return not(v1 == v2);
}

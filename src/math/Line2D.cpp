#include "Line2D.hpp"

#include "math/MathRegex.hpp"
#include "util/Exception.hpp"
#include "util/Serialization.hpp"


Line2D Line2D::from_string(std::string_view str) noexcept(false)
{
    auto match = math_regex::Line2D<0, 1>::match(str);
    if (not match)
    {
        throw SerializationError{str};
    }

    return Line2D{Convert<Point2D>::from_string(match[0].to<std::string_view>()),
                  Convert<Point2D>::from_string(match[1].to<std::string_view>())};
}

std::string Line2D::to_string() const
{
    // format:
    // @Line2D{ @Point2D{ 3.14, -5 }, @Point2D{ 3.14, -5 } }
    return "@Line2D{ " + Convert<Point2D>::to_string(first) + ", " + Convert<Point2D>::to_string(second) + " }";
}

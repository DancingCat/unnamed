#include <iostream>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "Application.hpp"
#include "ui/FontCache.hpp"


namespace
{
    class SdlLib
    {
        bool sdl = false;
        bool img = false;
        bool ttf = false;

    public:
        SdlLib()
        {
            sdl = SDL_Init(SDL_INIT_EVERYTHING) == 0;
            if (not sdl)
            {
                std::cout << "Failed to init_render_params SDL2:\n"
                          << SDL_GetError() << std::endl;
                return;
            }

            img = IMG_Init(IMG_INIT_PNG) != 0;
            if (not img)
            {
                std::cout << "Failed to init_render_params SDL2 Image:\n"
                          << IMG_GetError() << std::endl;
                return;
            }

            ttf = TTF_Init() == 0;
            if (not ttf)
            {
                std::cout << "Failed to init_render_params SDL2 TTF:\n"
                          << TTF_GetError() << std::endl;
                return;
            }
        }

        ~SdlLib()
        {
            if (ttf)
            {
                TTF_Quit();
            }

            if (img)
            {
                IMG_Quit();
            }

            if (sdl)
            {
                SDL_Quit();
            }
        }

        operator bool() const
        {
            return sdl and img and ttf;
        }
    };
} // namespace


int main(int argc, char **argv)
{
    SdlLib lib;

    if (not lib)
    {
        return -1;
    }

    Application app;
    FontCache font_cache;

    return app.execute();
}

#include "Level.hpp"

#include <cassert>


Level *Level::current = nullptr;


Level::Part::Part(Texture texture, bool collision)
    : texture(std::move(texture))
    , collision(collision)
{
}


bool Level::Part::has_collision() const
{
    return collision;
}


void Level::Part::set_collision(bool value)
{
    collision = value;
}


Texture Level::Part::get_texture() const
{
    return texture;
}


void Level::Part::set_texture(Texture newTexture)
{
    texture = std::move(newTexture);
}


Level::Level(int width, int height)
{
    assert(width > 0 and height > 0);

    parts.resize(width);
    for (auto &&line : parts)
    {
        line.resize(height);
    }
}


int Level::get_width() const
{
    return parts.size();
}


int Level::get_height() const
{
    return parts.front().size();
}


Level::Part *Level::get_part_at(int x, int y)
{
    if (x < 0 or y < 0 or x >= get_width() or y >= get_height())
    {
        return nullptr;
    }

    return &parts[x][y];
}


const Level::Part *Level::get_part_at(int x, int y) const
{
    return const_cast<Level *>(this)->get_part_at(x, y);
}


Level::~Level()
{
    if (current == this)
    {
        current = nullptr;
    }
}

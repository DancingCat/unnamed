#pragma once

#include "math/Point2D.hpp"


/**
 * @brief Камера
 */
class Camera
{
    Point2D center = Point2D::null();
    double scale = 20;

public:
    /**
     * @brief Камера
     */
    Camera() = default;

    /**
     * @brief Получить положение центра
     */
    Point2D get_center() const
    {
        return center;
    }

    /**
     * @brief Установить положение центра
     */
    inline void set_center(Point2D value)
    {
        center = value;
    }

    /**
     * @brief Получить масштаб (тайлов на высоту экрана)
     */
    inline double get_scale() const
    {
        return scale;
    }

    /**
     * @brief Установить масштаб (тайлов на высоту экрана)
     */
    inline void set_scale(double value)
    {
        Point2D pos = get_center();
        scale = value;
        set_center(pos);
    }
};

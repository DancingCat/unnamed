#pragma once

#include "game/Entity.hpp"
#include "game/GeometryObject.hpp"


/**
 * @brief Игрок
 */
class Player : public GeometryObject,
               public Entity
{
public:
    /**
     * @brief Игрок
     * @param position Начальное положение
     */
    Player(Point2D position);
};

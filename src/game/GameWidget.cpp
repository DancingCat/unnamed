#include "GameWidget.hpp"

#include "ui/Painter.hpp"


GameWidget::GameWidget(Vector2D size)
    : Widget(Rect2D(Point2D::null(), size))
    , render_params(*this, game.get_camera())
{
    render_params.update();
    game.get_crosshair().init_render_params(&render_params);
}


void GameWidget::mouse_button_event(const MouseButtonEvent &event)
{
    if (not event.pressed or event.button != MouseButton::Left)
    {
        return;
    }

    game.move_player_to_target();
    // game.get_player().set_target_position(render_params.translate_to_model_coords(Point2D(event.x, event.y)));
}


void GameWidget::mouse_move_event(const MouseMoveEvent &event)
{
    game.get_crosshair().set_screen_position(Point2D(event.x, event.y));
}


void GameWidget::mouse_in_event(MouseInEvent event)
{
}


void GameWidget::mouse_out_event(MouseOutEvent event)
{
}


void GameWidget::key_press_event(const KeyPressEvent &event)
{
}


void GameWidget::paint_event(PaintEvent event)
{
    Painter painter(this);

    render_level(painter);
    render_player(painter);

    painter.set_color({0x00, 0xff, 0x00});
    painter.fill_rect(Rect2D::create_by_center(game.get_crosshair().get_screen_position(),
                                               Vector2D(15, 15)));
}


void GameWidget::tick_event(const TickEvent &event)
{
    game.update(event.time_delta);
    render_params.update();
}


void GameWidget::render_level(Painter &painter)
{
    Level *current_level = game.get_current_level();

    Point2D start = render_params.get_camera_left_top().trunc();
    Point2D end = render_params.get_camera_right_bottom().ceil();
    Vector2D offset = Vector2D::from_points(start, render_params.get_camera_left_top());

    for (int x = start.x; x <= end.x; ++x)
    {
        for (int y = start.y; y <= end.y; ++y)
        {
            Level::Part *part = current_level->get_part_at(x, y);
            if (part == nullptr)
            {
                continue;
            }

            painter.draw_texture(part->get_texture(),
                                 render_params.get_block(Point2D(x - start.x, y - start.y) - offset));
        }
    }
}


void GameWidget::render_player(Painter &painter)
{
    painter.set_color(Color{0x00, 0x00, 0xff});

    painter.fill_rect(render_params.translate_rect(Rect2D::create_by_center(game.get_player().get_position(),
                                                                            Vector2D(game.get_player().get_collision_radius(),
                                                                                     game.get_player().get_collision_radius()))));
}

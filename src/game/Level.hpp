#pragma once

#include <memory>
#include <vector>

#include "ui/Texture.hpp"


/**
 * @brief Уровень
 */
class Level
{
public:
    class Part;

private:
    static Level *current;
    std::vector<std::vector<Part>> parts;

public:
    /**
     * @brief Уровень
     * @param width Количество тайлов в ширину
     * @param height Количество тайлов в высоту
     *
     * @note Высота и ширина должны быть больше нуля, размер уровня задается при создании
     *       Уровень всегда имеет прямоугольную форму
     */
    Level(int width, int height);

    ~Level();

    /**
     * @brief Количество тайлов по ширине
     */
    int get_width() const;

    /**
     * @brief Количество тайлов по высоте
     */
    int get_height() const;

    /**
     * @brief Получить часть по номеру тайла
     * @param x Номер по ширине
     * @param y Номер по высоте
     * @return Указатель на тайл, если координаты выходят за границы уровня - nullptr
     */
    Part *get_part_at(int x, int y);

    /**
     * @brief Получить часть по номеру тайла
     * @param x Номер по ширине
     * @param y Номер по высоте
     * @return Указатель на тайл, если координаты выходят за границы уровня - nullptr
     */
    const Part *get_part_at(int x, int y) const;

    Level(Level &) = delete;
    Level(Level &&) = delete;
    Level &operator=(Level &) = delete;
    Level &operator=(Level &&) = delete;
};


/**
 * @brief Часть уровня (тайл)
 */
class Level::Part
{
    bool collision = false;
    Texture texture = Texture::null();

public:
    Part() = default;

    /**
     * @brief Часть уровня (тайл)
     * @param texture Текстура
     * @param collision Имеется ли коллизия
     */
    Part(Texture texture, bool collision);

    /**
     * @brief Имеется ли коллизия
     */
    bool has_collision() const;

    /**
     * @brief Установить коллизию
     */
    void set_collision(bool value = true);

    /**
     * @brief Получить текущую текстуру
     */
    Texture get_texture() const;

    /**
     * @brief Установить новую текстуру
     */
    void set_texture(Texture newTexture);
};

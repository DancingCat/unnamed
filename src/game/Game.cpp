#include "Game.hpp"

#include "math/MathCollision.hpp"


namespace
{
    std::shared_ptr<Level> make_test_level()
    {
        Texture grass = Texture::load_from_file("texture/test/Grass.png");
        Texture stone = Texture::load_from_file("texture/test/Stone.png");
        // Texture bricks = Texture::load_from_file("texture/test/Bricks.png");
        // Texture paving_stone = Texture::load_from_file("texture/test/PavingStone.png");

        std::shared_ptr level = std::make_shared<Level>(25, 25);

        for (int x = 1, w = level->get_width() - 1; x < w; ++x)
        {
            for (int y = 1, h = level->get_height() - 1; y < h; ++y)
            {
                Level::Part *part = level->get_part_at(x, y);
                part->set_texture(grass);
                part->set_collision(false);
            }
        }

        auto set_stone_wall = [&stone](Level::Part *part) {
            part->set_texture(stone);
            part->set_collision();
        };

        for (int x = 0, w = level->get_width(); x < w; ++x)
        {
            set_stone_wall(level->get_part_at(x, 0));
            set_stone_wall(level->get_part_at(x, level->get_height() - 1));
        }

        for (int y = 1, h = level->get_height(); y < h; ++y)
        {
            set_stone_wall(level->get_part_at(0, y));
            set_stone_wall(level->get_part_at(level->get_width() - 1, y));
        }

        for (int i = 0; i < 10; ++i)
        {
            set_stone_wall(level->get_part_at(5 + i, 5));
        }

        return level;
    }
} // namespace


Game::Game()
    : player(Point2D(2, 2))
{
    current_level = make_test_level();
}


void Game::update(double time_delta)
{
    update_player(time_delta);
    update_camera();
    update_crosshair();
}


void Game::update_player(double time_delta)
{
    if (Point2D::equals(player.get_position(), player.get_target_position()))
    {
        return;
    }

    constexpr double speed = 5;

    Point2D player_pos = player.get_position()
                       + Vector2D::from_points(player.get_position(),
                                               player.get_target_position())
                                 .normalized()
                             * (speed * time_delta);

    if (Vector2D::from_points(player.get_position(), player.get_target_position()).get_length()
        < Vector2D::from_points(player.get_position(), player_pos).get_length())
    {
        player_pos = player.get_target_position();
    }

    Level::Part *part = current_level->get_part_at(player_pos.x, player_pos.y);

    if (part == nullptr)
    {
        return;
    }

    if (part->has_collision())
    {
        return;
    }

    player.set_position(player_pos);
}


void Game::update_camera()
{
    camera.set_center(player.get_position());
}


void Game::update_crosshair()
{
}

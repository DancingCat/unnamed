#pragma once

#include "game/Camera.hpp"
#include "game/Crosshair.hpp"
#include "game/Level.hpp"
#include "game/Player.hpp"


class Painter;


class Game
{
    std::shared_ptr<Level> current_level;
    Camera camera;
    Crosshair crosshair;
    Player player;

public:
    Game();

    inline Player &get_player()
    {
        return player;
    }

    inline Level *get_current_level()
    {
        return current_level.get();
    }

    inline Camera &get_camera()
    {
        return camera;
    }

    inline Crosshair &get_crosshair()
    {
        return crosshair;
    }

    inline void move_player_to_target()
    {
        player.set_target_position(crosshair.get_model_position());
    }

    void update(double time_delta);

    Game(Game &) = delete;
    Game(Game &&) = delete;
    Game &operator=(Game &) = delete;
    Game &operator=(Game &&) = delete;

private:
    void update_player(double time_delta);
    void update_camera();
    void update_crosshair();
};

#include "RenderParams.hpp"

#include "game/Camera.hpp"
#include "ui/Widget.hpp"


RenderParams::RenderParams(Widget &target, Camera &camera)
    : target(target)
    , camera(camera)
{
}


void RenderParams::update()
{
    Vector2D size = target.get_size();
    double scale = camera.get_scale();

    block_size = size.dy / scale;

    blocks_per_width = scale * (size.dx / size.dy);
    blocks_per_height = scale;

    camera_left_top = camera.get_center();
    camera_left_top.x -= blocks_per_width / 2;
    camera_left_top.y -= blocks_per_height / 2;
}


Point2D RenderParams::translate_to_model_coords(Point2D point) const
{
    point.x = (point.x * blocks_per_width) / target.get_size().dx + camera_left_top.x;
    point.y = (point.y * blocks_per_height) / target.get_size().dy + camera_left_top.y;

    return point;
}


Point2D RenderParams::translate_from_model_coords(Point2D point) const
{
    point.x = (point.x * target.get_size().dx) / blocks_per_width - camera_left_top.x;
    point.y = (point.y * target.get_size().dy) / blocks_per_height - camera_left_top.y;

    return point;
}

#pragma once

#include "math/Rect2D.hpp"


class Widget;
class Camera;


/**
 * @brief Параметры рендера
 */
class RenderParams
{
    Widget &target;
    Camera &camera;

    double block_size = 0;
    double blocks_per_width = 0;
    double blocks_per_height = 0;
    Point2D camera_left_top = Point2D::null();

public:
    /**
     * @brief Параметры рендера
     */
    RenderParams(Widget &target, Camera &camera);

    /**
     * @brief Получить верхний левый угол камеры (в игровых координатах)
     */
    inline Point2D get_camera_left_top() const
    {
        return camera_left_top;
    }

    /**
     * @brief Получить нижний правый угол камеры (в игровых координатах)
     */
    inline Point2D get_camera_right_bottom() const
    {
        return Point2D(camera_left_top.x + blocks_per_width,
                       camera_left_top.y + blocks_per_height);
    }

    /**
     * @brief Обновить параметры
     */
    void update();

    /**
     * @brief Получить прямоугольник для рендера тайла
     * @param pos Позиция тайла
     */
    inline Rect2D get_block(Point2D pos) const
    {
        return Rect2D(Point2D{pos.x * block_size, pos.y * block_size},
                      Vector2D{block_size, block_size});
    }

    /**
     * @brief Получить прямоугольник для рендера
     * @param pos Центр прямоугольника
     * @param size Размер прямоугольника
     */
    inline Rect2D get_rect_of(Point2D pos, Vector2D size) const
    {
        return Rect2D(Point2D{(pos.x - camera_left_top.x) * block_size,
                              (pos.y - camera_left_top.y) * block_size},
                      size * block_size);
    }

    /**
     * @brief Трансформировать прямоугольник из игровых координат в координаты для рендера
     */
    inline Rect2D translate_rect(const Rect2D &rect) const
    {
        return get_rect_of(rect.get_left_top(), rect.get_size());
    }

    /**
     * @brief Трансформировать точку в модельные координаты
     */
    Point2D translate_to_model_coords(Point2D point) const;

    /**
     * @brief Получить точку для рендера из модельных координат
     */
    Point2D translate_from_model_coords(Point2D point) const;
};

#pragma once

#include "game/Game.hpp"
#include "game/RenderParams.hpp"
#include "ui/Widget.hpp"


class MainMenu;


/**
 * @brief Виджет игры
 */
class GameWidget : public Widget
{
    Game game;
    RenderParams render_params;

public:
    /**
     * @brief Виджет игры
     * @param size Размер (определяется окном)
     */
    GameWidget(Vector2D size);

protected:
    /**
     * @brief Событие клавиши мыши
     * @param event Информация о событии
     */
    void mouse_button_event(const MouseButtonEvent &event) override;

    /**
     * @brief Событие перемещения мыши
     * @param event Информация о событии
     */
    void mouse_move_event(const MouseMoveEvent &event) override;

    /**
     * @brief Событие захода курсора внутрь виджета
     */
    void mouse_in_event(MouseInEvent) override;

    /**
     * @brief Событие выхода курсора из виджета
     */
    void mouse_out_event(MouseOutEvent) override;

    /**
     * @brief Событие кнопки клавиатуры
     * @param event Информация о событии
     */
    void key_press_event(const KeyPressEvent &event) override;

    /**
     * @brief Событие отрисовки
     */
    void paint_event(PaintEvent) override;

    /**
     * @brief Событие тика (обновление модели)
     * @param event Информация о событии
     */
    void tick_event(const TickEvent &event) override;

private:
    void render_level(Painter &painter);

    void render_player(Painter &painter);
};

#pragma once

#include "math/Point2D.hpp"
#include "math/Vector2D.hpp"


/**
 * @brief Геометрический объект
 */
class GeometryObject
{
    Point2D position;
    Point2D target_position;
    Vector2D speed;
    double collision_radius;

public:
    /**
     * @brief Геометрический объект
     * @param position Позиция
     * @param collision_radius Радиус коллизии
     */
    inline GeometryObject(Point2D position, double collision_radius)
        : position(position)
        , target_position(position)
        , speed(Vector2D::null())
        , collision_radius(collision_radius)
    {
    }

    /**
     * @brief Получить позицию
     */
    inline Point2D get_position() const
    {
        return position;
    }

    /**
     * @brief Установить позицию
     */
    inline void set_position(Point2D value)
    {
        this->position = value;
    }

    /**
     * @brief Получить целевую позицию для перемещения
     */
    inline Point2D get_target_position() const
    {
        return target_position;
    }

    /**
     * @brief Задать целевую позицию для перемещения
     */
    inline void set_target_position(Point2D value)
    {
        target_position = value;
    }

    /**
     * @brief Получить скорость
     */
    inline Vector2D get_speed() const
    {
        return speed;
    }

    /**
     * @brief Установить скорость
     */
    inline void set_speed(Vector2D value)
    {
        this->speed = value;
    }

    /**
     * @brief Получить радиус коллизии
     */
    inline double get_collision_radius() const
    {
        return collision_radius;
    }

    /**
     * @brief Установить радиус коллизии
     */
    inline void set_collision_radius(double value)
    {
        collision_radius = value;
    }
};

#include "game/RenderParams.hpp"
#include "math/Point2D.hpp"

#include <cassert>


/**
 * @brief Перекрестие
 *
 * Отвечает за расположение курсора мыши в игровом пространстве для контроля игрока.
 *
 * @note Класс имеет частичную инициализацию для отвязки зависимости от инициализации
 *       параметров рендера. Для управления необходима инициализация через
 *       Crosshair::init_render_params.
 */
class Crosshair
{
    Point2D screen_position = Point2D::null();
    RenderParams *render_params = nullptr;

public:
    /**
     * @brief Перекрестие
     */
    Crosshair() = default;

    /**
     * @brief Инициализация параметров рендера.
     */
    inline void init_render_params(RenderParams *params)
    {
        render_params = params;
    }

    /**
     * @brief Получить текущие модельные координаты
     */
    inline Point2D get_model_position() const
    {
        assert(render_params != nullptr);

        return render_params->translate_to_model_coords(screen_position);
    }

    /**
     * @brief Установить модельные координаты
     */
    inline void set_model_position(Point2D value)
    {
        assert(render_params != nullptr);

        screen_position = render_params->translate_from_model_coords(value);
    }

    /**
     * @brief Получение положения в экранных координатах
     */
    inline Point2D get_screen_position() const
    {
        return screen_position;
    }

    /**
     * @brief Установка положения в экранных координатах
     */
    inline void set_screen_position(Point2D value)
    {
        screen_position = value;
    }
};

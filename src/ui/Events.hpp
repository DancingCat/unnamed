#pragma once

#include <SDL2/SDL_keycode.h>


/**
 * @brief Клавиша мыши
 */
enum class MouseButton
{
    Left,     ///< Левая
    Right,    ///< Правая

    Undefined ///< Неопределенная (не отслеживается)
};


/**
 * @brief Событие клавиши мыши
 */
struct MouseButtonEvent
{
    int x;              ///< Текущая позиция по Х
    int y;              ///< Текущая позиция по У
    MouseButton button; ///< Клавиша мыши
    bool pressed;       ///< Нажата/отжата
};


/**
 * @brief Событие перемещения мыши
 */
struct MouseMoveEvent
{
    int x; ///< Текущая позиция по Х
    int y; ///< Текущая позиция по У
};


/**
 * @brief Событие кнопки клавиатуры
 */
struct KeyPressEvent
{
    SDL_Scancode key; ///< Код кнопки
    bool pressed;     ///< Нажата/отжата
};


/**
 * @brief Событие отрисовки (заглушка)
 */
struct PaintEvent
{
};


/**
 * @brief Событие захода курсора внутрь виджета (заглушка)
 */
struct MouseInEvent
{
};


/**
 * @brief Событие выхода курсора из виджета (заглушка)
 */
struct MouseOutEvent
{
};


/**
 * @brief Событие тика
 */
struct TickEvent
{
    double time_delta; ///< Сколько прошло времени с последнего тика (сек)
};

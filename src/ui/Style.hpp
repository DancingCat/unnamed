#pragma once

#include <string_view>

#include "ui/Color.hpp"


/// Базовый стиль всех виджетов
template <typename = void>
struct Style
{
    /// Цвет заднего фона
    static constexpr Color background_color = {0xDD, 0xDD, 0xDD};
    /// Цвет переднего плана
    static constexpr Color foreground_color = {0x20, 0x20, 0x20};

    /// Цвет подсветки при наведении
    static constexpr Color hover_color = {0xFF, 0xB6, 0x4D, 0x80};
    /// Цвет выключенного виджета
    static constexpr Color disabled_color = {0x80, 0x80, 0x80, 0x80};

    /// Файл шрифта
    static constexpr auto font_name = "font/UbuntuMono-Regular.ttf";
    /// Размер шрифта
    static constexpr int font_size = 32;
};

/// Специализация для CheckBox
template <>
struct Style<class CheckBox> : public Style<void>
{
    /// Цвет включенного чека
    static constexpr Color checked_color = {0x81, 0xB3, 0xD9};

    /// Отступ кнопки от края относительно высоты виджета
    static constexpr double button_offset = 0.1;
};

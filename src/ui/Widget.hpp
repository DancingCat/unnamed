#pragma once

#include <memory>
#include <optional>
#include <vector>

#include "Events.hpp"
#include "ui/layout/Layout.hpp"

#include "math/Point2D.hpp"
#include "math/Rect2D.hpp"


/**
 * @brief Базовый класс виджета
 */
class Widget
{
    Rect2D rect;
    bool enabled = true;
    bool visible = true;
    bool compose_lock = false;

    mutable std::optional<Point2D> abs_pos;

    Widget *parent = nullptr;
    std::vector<Widget *> children;

    layout::Layout layout;

public:
    /**
     * @brief Базовый класс виджета
     * @param rect Расположение и размер
     */
    explicit Widget(const Rect2D &rect = Rect2D::null());

    virtual ~Widget();

    /**
     * @brief Добавить дочерний виджет
     * @param widget Указатель на виджет
     * @return Добавленный виджет
     *
     * Если дочерний виджет уже имеет родителя, то он будет исключен из списка дочерних виджетов у родителя
     */
    Widget *add_child(Widget *widget);

    /**
     * @brief Получить родительский виджет
     */
    Widget *get_parent() const;

    /**
     * @brief Обойти всех детей
     * @param func Функция обратного вызова
     */
    template <typename Func>
    void for_each_child(Func &&func) const
    {
        for (auto &&child : children)
        {
            func(child);
        }
    }

    /**
     * @brief Активность
     */
    bool is_enabled() const;

    /**
     * @brief Установить активность
     */
    void set_enabled(bool value);

    /**
     * @brief Видимость
     */
    bool is_visible() const;

    /**
     * @brief Установить видимость
     */
    void set_visible(bool value);

    /**
     * @brief  Расположение и размер
     */
    Rect2D get_rect() const;

    /**
     * @brief Изменить расположение
     */
    void set_position(Point2D position);

    /**
     * @brief Расположение
     */
    Point2D get_position() const;

    /**
     * @brief Абсолютное расположение
     */
    Point2D get_absolute_position() const;

    /**
     * @brief Установить размер
     */
    void set_size(Vector2D size);

    /**
     * @brief Размер
     */
    Vector2D get_size() const;

    /**
     * @brief Установить компоновщик
     */
    void set_layout(layout::Layout layout);

protected:
    /**
     * @brief Событие клавиши мыши
     * @param event Информация о событии
     */
    virtual void mouse_button_event(const MouseButtonEvent &event);

    /**
     * @brief Событие перемещения мыши
     * @param event Информация о событии
     */
    virtual void mouse_move_event(const MouseMoveEvent &event);

    /**
     * @brief Событие захода курсора внутрь виджета
     */
    virtual void mouse_in_event(MouseInEvent);

    /**
     * @brief Событие выхода курсора из виджета
     */
    virtual void mouse_out_event(MouseOutEvent);

    /**
     * @brief Событие кнопки клавиатуры
     * @param event Информация о событии
     */
    virtual void key_press_event(const KeyPressEvent &event);

    /**
     * @brief Событие отрисовки
     */
    virtual void paint_event(PaintEvent);

    /**
     * @brief Событие тика (обновление модели)
     * @param event Информация о событии
     */
    virtual void tick_event(const TickEvent &event);

private:
    void dispatch_event(MouseButtonEvent &event);
    void dispatch_event(MouseMoveEvent &event);
    void dispatch_event(KeyPressEvent &event);
    void dispatch_event(PaintEvent);
    void dispatch_event(TickEvent &event);

    Widget *find_widget_at(Point2D point, bool checkVisible, bool checkEnabled) const;
    void drop_absolute_position_cache();

    void compose();

    friend class Window;
    friend class Application;
};

#pragma once

#include <functional>

#include "ui/Texture.hpp"
#include "ui/Widget.hpp"


/**
 * @brief Главное меню
 */
class MainMenu : public Widget
{
    Texture image;

public:
    /**
     * @brief Главное меню
     */
    MainMenu(Vector2D size);

    /// Обработчик нажатия кнопки "Старт"
    std::function<void(void)> on_start_clicked;

    /// Обработчик нажатия кнопки "Настройки"
    std::function<void(void)> on_options_clicked;

    /// Обработчик нажатия кнопки "Выход"
    std::function<void(void)> on_exit_clicked;

    MainMenu(MainMenu &) = delete;
    MainMenu(MainMenu &&) = delete;
    MainMenu &operator=(MainMenu &) = delete;
    MainMenu &operator=(MainMenu &&) = delete;

protected:
    void paint_event(PaintEvent) override;
};

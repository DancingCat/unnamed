#pragma once

#include <functional>
#include <string>

#include "ui/Texture.hpp"
#include "ui/Widget.hpp"


/// Кнопка
class Button : public Widget
{
    bool hover = false;
    std::string text;
    Texture text_texture;

public:
    /**
     * @brief Кнопка
     * @param position Расположение (верхний левый угол)
     * @param size Размер (px)
     */
    Button(Point2D position, Vector2D size);

    /**
     * @brief Кнопка
     * @param position Расположение (верхний левый угол)
     * @param size Размер (px)
     */
    explicit Button(Vector2D size);

    /**
     * @brief Callback при клике на кнопку
     */
    std::function<void(void)> clicked;

    /**
     * @brief Установить текст
     */
    void set_text(std::string text);

protected:
    /**
     * @brief Событие клавиши мыши
     * @param event Информация о событии
     */
    void mouse_button_event(const MouseButtonEvent &event) override;

    /**
     * @brief Событие отрисовки
     * @param event Информация о событии
     */
    void paint_event(PaintEvent) override;

    /**
     * @brief Событие захода курсора внутрь виджета
     */
    void mouse_in_event(MouseInEvent) override;

    /**
     * @brief Событие выхода курсора из виджета
     */
    void mouse_out_event(MouseOutEvent) override;
};

#include "MainMenu.hpp"

#include <iostream>

#include "Application.hpp"
#include "ui/Button.hpp"
#include "ui/CheckBox.hpp"
#include "ui/Painter.hpp"
#include "ui/layout/no_resize/Anchor.hpp"
#include "ui/layout/no_resize/Vertical.hpp"


MainMenu::MainMenu(Vector2D size)
    : Widget(Rect2D(Point2D(100, 100), Vector2D(size.dx - 200, size.dy - 200)))
    , image(Texture::null())
{
    Vector2D child_size{250, 150};

    auto *start = (Button *) add_child(new Button(child_size));
    start->set_text("Start");
    start->clicked = [this]() {
        if (on_start_clicked)
        {
            on_start_clicked();
        }
    };

    auto *options = (Button *) add_child(new Button(child_size));
    options->set_text("Options");
    options->clicked = [this]() {
        if (on_options_clicked)
        {
            on_options_clicked();
        }
    };

    auto *exit = (Button *) add_child(new Button(child_size));
    exit->set_text("Exit");
    exit->clicked = [this]() {
        if (on_exit_clicked)
        {
            on_exit_clicked();
        }
    };

    constexpr layout::Offset offset = {.value = 100, .type = layout::Offset::Absolute};

    using layout::Layout;
    using layout::no_resize::Anchor;
    using layout::no_resize::Vertical;

    set_layout(Vertical{offset, offset} and Anchor{Anchor::Left, offset});
}


void MainMenu::paint_event(PaintEvent)
{
    Painter painter{this};

    painter.set_color({0, 0xFF, 0});
    painter.draw_rect(Rect2D{
        {0, 0},
        get_size()
    });

    if (image.is_null())
    {
        image = Texture::load_from_file("image.jpg");
    }

    painter.draw_texture(image, Rect2D{
                                    {get_size().dx / 2, get_size().dy / 2},
                                    {get_size().dy / 2, get_size().dy / 2}
    });
}

#pragma once

#include <string>

#include "ui/Widget.hpp"


struct SDL_Window;
struct SDL_Renderer;


/**
 * @brief Окно приложения
 */
class Window : public Widget
{
    Widget *under_mouse = nullptr;           ///< Виджет под курсором
    Widget *keyboard_focus = nullptr;        ///< Виджет с клавиатурном фокусом

    std::string title;                       ///< Заголовок окна

    SDL_Window *window_handle = nullptr;     ///< Указатель на окно
    SDL_Renderer *renderer_handle = nullptr; ///< Указатель на рендерер

public:
    /**
     * @brief Окно приложения
     *
     * @param title Заголовок
     * @param size Размер окна
     */
    explicit Window(std::string title, Vector2D size);

    ~Window();

    /**
     * @brief Установить виджет под курсором
     */
    void set_widget_under_mouse(Widget *widget);

    /**
     * @brief Получить виджет под курсором
     */
    inline Widget *get_widget_under_mouse() const
    {
        return under_mouse;
    }

    /**
     * @brief Установить виджет в фокус клавиатуры
     */
    void set_keyboard_focus(Widget *widget);

    /**
     * @brief Получить виджет в фокусе клавиатуры
     */
    inline Widget *get_keyboard_focus() const
    {
        return keyboard_focus;
    }

    /**
     * @brief Получить объект рендерера
     */
    inline SDL_Renderer *get_renderer() const
    {
        return renderer_handle;
    }
};

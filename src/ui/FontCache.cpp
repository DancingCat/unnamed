#include "FontCache.hpp"

#include <cassert>
#include <iostream>


FontCache *FontCache::instance = nullptr;


FontCache &FontCache::get_instance()
{
    assert(instance != nullptr);
    return *instance;
}


FontCache::FontCache()
{
    assert(instance == nullptr);
    instance = this;
}


FontCache::~FontCache()
{
    for (auto &&[name, font] : data)
    {
        TTF_CloseFont(font);
    }

    data.clear();

    instance = nullptr;
}


TTF_Font *FontCache::load(std::string_view fontName, int size)
{
    auto it = data.find({fontName, size});

    if (it == data.end())
    {
        TTF_Font *ptr = TTF_OpenFont(fontName.data(), size);

        if (ptr == nullptr)
        {
            std::cout << "Failed to load font '" << fontName << "':" << std::endl
                      << TTF_GetError() << std::endl;
        }

        auto &&[i, flag] = data.insert({
            {fontName, size},
            ptr
        });
        it = i;
    }

    return it->second;
}

#include "Texture.hpp"

#include <SDL.h>
#include <SDL_image.h>

#include "Application.hpp"
#include "ui/Font.hpp"
#include "ui/Window.hpp"


namespace
{
    constexpr auto deleter = [](SDL_Texture *ptr) {
        if (ptr != nullptr)
        {
            SDL_DestroyTexture(ptr);
        }
    };
} // namespace


Texture Texture::render_text(Font font, std::string_view str, Color color)
{
    if (font.get_handle() == nullptr)
    {
        return Texture::null();
    }

    SDL_Color fg = {.r = color.red, .g = color.green, .b = color.blue, .a = color.alpha};

    SDL_Surface *surface = TTF_RenderText_Blended(font.get_handle(), str.data(), fg);
    if (surface == nullptr)
    {
        return Texture::null();
    }

    Window *window = Application::get_instance().get_window();
    if (window == nullptr)
    {
        return Texture::null();
    }

    SDL_Texture *texture = SDL_CreateTextureFromSurface(window->get_renderer(), surface);
    SDL_FreeSurface(surface);
    if (texture == nullptr)
    {
        return Texture::null();
    }

    return Texture{texture};
}


Texture Texture::load_from_file(const std::filesystem::path &path)
{
    Window *window = Application::get_instance().get_window();
    if (window == nullptr)
    {
        return Texture::null();
    }

    SDL_Texture *texture = IMG_LoadTexture(window->get_renderer(), path.c_str());

    if (texture == nullptr)
    {
        return Texture::null();
    }

    return Texture{texture};
}


Texture Texture::null()
{
    return Texture{nullptr};
}


Texture::Texture(SDL_Texture *texture)
    : handle(texture, deleter)
{
}


bool Texture::is_null() const
{
    return handle == nullptr;
}


Vector2D Texture::get_size() const
{
    if (is_null())
    {
        return Vector2D{0, 0};
    }

    int w, h;
    SDL_QueryTexture(handle.get(), nullptr, nullptr, &w, &h);
    return Vector2D{static_cast<double>(w), static_cast<double>(h)};
}


SDL_Texture *Texture::get_handle() const
{
    return handle.get();
}


void Texture::release()
{
    handle = nullptr;
}

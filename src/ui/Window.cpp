#include "Window.hpp"

#include <iostream>

#include <SDL.h>

#include "ui/Painter.hpp"


Window::Window(std::string title, Vector2D size)
    : Widget(Rect2D(Point2D::null(), size))
    , title(std::move(title))
{
    window_handle = SDL_CreateWindow(this->title.c_str(),
                                     SDL_WINDOWPOS_CENTERED,
                                     SDL_WINDOWPOS_CENTERED,
                                     (int) size.dx,
                                     (int) size.dy,
                                     SDL_WINDOW_SHOWN);

    if (window_handle == nullptr)
    {
        std::cout << "Failed to create window:\n"
                  << SDL_GetError() << std::endl;
        return;
    }

    renderer_handle = SDL_CreateRenderer(window_handle,
                                         -1,
                                         SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer_handle == nullptr)
    {
        std::cout << "Failed to create renderer:\n"
                  << SDL_GetError() << std::endl;
        return;
    }

    SDL_SetRenderDrawBlendMode(renderer_handle, SDL_BLENDMODE_BLEND);

    set_keyboard_focus(this);
}


Window::~Window()
{
    if (renderer_handle)
    {
        SDL_DestroyRenderer(renderer_handle);
    }

    if (window_handle)
    {
        SDL_DestroyWindow(window_handle);
    }
}


void Window::set_widget_under_mouse(Widget *widget)
{
    if (under_mouse)
    {
        under_mouse->mouse_out_event({});
    }

    under_mouse = widget;

    if (under_mouse)
    {
        under_mouse->mouse_in_event({});
    }
}


void Window::set_keyboard_focus(Widget *widget)
{
    keyboard_focus = widget;
}

#pragma once

#include <filesystem>
#include <memory>

#include "math/Vector2D.hpp"
#include "ui/Color.hpp"


struct SDL_Texture;

class Painter;
class Font;


/**
 * @brief Текстура, обеспечивает распределенное владение ресурсом
 *
 * Является оберткой над ресурсом. Копирование/перемещение объекта не приводят к копированию ресурса или передаче
 * владения ресурсом. Владение обеспечивается распределенное. Получение указателя на ресурс через вызов функции
 * get_handle() необходим для использования внутри библиотеки.
 */
class Texture
{
    std::shared_ptr<SDL_Texture> handle;

public:
    /**
     * @brief Создать текстуру с текстом
     * @param font Шрифт
     * @param str Текст
     * @param color Цвет текста
     * @return Текстура, пустая в случае ошибки
     *
     * @details Создает текстуру с текстом указанного размера, шрифта и цвета. Фон текстуры прозрачный.
     */
    static Texture render_text(Font font, std::string_view str, Color color);

    /**
     * @brief Загрузить текстуру из файла
     * @param path Путь к файлу
     * @return Текстура, пустая в случае ошибки
     */
    static Texture load_from_file(const std::filesystem::path &path);

    /**
     * @brief Создать пустую текстуру
     */
    static Texture null();

    /**
     * @brief Текстура
     * @param texture Указатель на ресурс
     */
    explicit Texture(SDL_Texture *texture);

    /**
     * @brief Является ли текстура пустой
     */
    bool is_null() const;

    /**
     * @brief Размер
     */
    Vector2D get_size() const;

    /**
     * @brief Получить невладеющий указатель на ресурс
     */
    SDL_Texture *get_handle() const;

    /**
     * @brief Отказаться от владения ресурсом
     */
    void release();
};

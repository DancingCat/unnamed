#include "Button.hpp"

#include "ui/Font.hpp"
#include "ui/Painter.hpp"
#include "ui/Style.hpp"


Button::Button(Point2D position, Vector2D size)
    : Widget(Rect2D{position, size})
    , text_texture(Texture::null())
{
}


Button::Button(Vector2D size)
    : Button(Point2D::null(), size)
{
}


void Button::set_text(std::string text)
{
    this->text = std::move(text);
    text_texture.release();
}


void Button::mouse_button_event(const MouseButtonEvent &event)
{
    if (clicked and event.pressed)
    {
        clicked();
    }
}


void Button::paint_event(PaintEvent)
{
    Painter painter{this};

    painter.set_color(Style<Button>::background_color);
    painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));

    if (not is_enabled())
    {
        painter.set_color(Style<Button>::disabled_color);
        painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));
    }
    else if (hover)
    {
        painter.set_color(Style<Button>::hover_color);
        painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));
    }

    if (text.empty())
    {
        return;
    }

    if (text_texture.is_null())
    {
        text_texture = Texture::render_text(
            Font{Style<Button>::font_name, Style<Button>::font_size},
            text,
            Style<Button>::foreground_color);
    }

    Vector2D t_size = text_texture.get_size();
    Vector2D size = get_size();
    Point2D t_pos = {size.dx / 2 - t_size.dx / 2, size.dy / 2 - t_size.dy / 2};

    Rect2D t_rect = {t_pos, t_size};
    painter.draw_texture(text_texture, t_rect);
}


void Button::mouse_in_event(MouseInEvent)
{
    hover = true;
}


void Button::mouse_out_event(MouseOutEvent)
{
    hover = false;
}

#pragma once

#include <functional>

#include "ui/Texture.hpp"
#include "ui/Widget.hpp"


/**
 * @brief Кнопка-переключатель
 */
class CheckBox : public Widget
{
    bool hover = false;
    bool checked = false;
    std::string text;
    Texture text_texture;

public:
    /**
     * @brief Кнопка-переключатель
     * @param position Расположение (верхний левый угол)
     * @param size Размер (px)
     */
    CheckBox(Point2D position, Vector2D size);

    /**
     * @brief Кнопка-переключатель
     * @param position Расположение (верхний левый угол)
     * @param size Размер (px)
     */
    CheckBox(Vector2D size);

    /**
     * @brief Callback при клике на кнопку
     *
     * Передает новое состояние переключателя
     */
    std::function<void(bool)> clicked;

    /**
     * @brief Установить текст
     */
    void set_text(std::string text);

    /**
     * @brief Получить состояние
     */
    bool is_checked() const;

    /**
     * @brief Установить состояние
     */
    void set_checked(bool value);

protected:
    /**
     * @brief Событие клавиши мыши
     * @param event Информация о событии
     */
    void mouse_button_event(const MouseButtonEvent &event) override;

    /**
     * @brief Событие отрисовки
     * @param event Информация о событии
     */
    void paint_event(PaintEvent) override;

    /**
     * @brief Событие захода курсора внутрь виджета
     */
    void mouse_in_event(MouseInEvent) override;

    /**
     * @brief Событие выхода курсора из виджета
     */
    void mouse_out_event(MouseOutEvent) override;
};

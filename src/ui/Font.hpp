#pragma once

#include <string_view>

#include <SDL_ttf.h>


/**
 * @brief Шрифт
 *
 * Не владеет ресурсом, получает указатель из статического хранилища
 */
class Font
{
    TTF_Font *handle;

public:
    /**
     * @brief Шрифт
     * @param fontName Название шрифта
     * @param size Размер шрифта
     */
    Font(std::string_view fontName, int size);

    /**
     * @brief Получить указатель на ресурс
     */
    TTF_Font *get_handle() const;
};

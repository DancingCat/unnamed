#include "Widget.hpp"

#include "Application.hpp"
#include "math/MathCollision.hpp"
#include "ui/Window.hpp"
#include "ui/layout/Layout.hpp"
#include "util/RaiiAction.hpp"

#include <cassert>
#include <utility>


void Widget::mouse_button_event(const MouseButtonEvent &event)
{
    if (parent != nullptr)
    {
        const_cast<MouseButtonEvent &>(event).x += (int) rect.get_left_top().x;
        const_cast<MouseButtonEvent &>(event).y += (int) rect.get_left_top().y;
        parent->mouse_button_event(event);
    }
}


void Widget::mouse_move_event(const MouseMoveEvent &event)
{
    if (parent != nullptr)
    {
        const_cast<MouseMoveEvent &>(event).x += (int) rect.get_left_top().x;
        const_cast<MouseMoveEvent &>(event).y += (int) rect.get_left_top().y;
        parent->mouse_move_event(event);
    }
}


void Widget::mouse_in_event(MouseInEvent)
{
    if (parent != nullptr)
    {
        parent->mouse_in_event({});
    }
}


void Widget::mouse_out_event(MouseOutEvent)
{
    if (parent != nullptr)
    {
        parent->mouse_out_event({});
    }
}


void Widget::key_press_event(const KeyPressEvent &event)
{
    if (parent != nullptr)
    {
        parent->key_press_event(event);
    }
}


void Widget::paint_event(PaintEvent)
{
}


void Widget::tick_event(const TickEvent &)
{
}


Rect2D Widget::get_rect() const
{
    return rect;
}


void Widget::set_position(Point2D position)
{
    rect.set_left_top(position);
    drop_absolute_position_cache();
    compose();
}


Point2D Widget::get_position() const
{
    return rect.get_left_top();
}


Point2D Widget::get_absolute_position() const
{
    if (not abs_pos.has_value())
    {
        if (parent == nullptr)
        {
            abs_pos = get_position();
        }
        else
        {
            Point2D parent_abs_pos = parent->get_absolute_position();
            Point2D pos = get_position();
            pos.x += parent_abs_pos.x;
            pos.y += parent_abs_pos.y;
            abs_pos = pos;
        }
    }

    return abs_pos.value();
}


void Widget::set_size(Vector2D size)
{
    rect.set_size(size);
    compose();
}


Vector2D Widget::get_size() const
{
    return rect.get_size();
}


void Widget::set_layout(layout::Layout layout)
{
    this->layout = std::move(layout);
    compose();
}


bool Widget::is_visible() const
{
    if (not visible)
    {
        return false;
    }

    if (parent != nullptr)
    {
        return parent->is_visible();
    }

    return true;
}


void Widget::set_visible(bool value)
{
    visible = value;
    for_each_child([value](Widget *child) { child->set_visible(value); });
}


bool Widget::is_enabled() const
{
    if (not enabled)
    {
        return false;
    }

    if (parent != nullptr)
    {
        return parent->is_enabled();
    }

    return true;
}


void Widget::set_enabled(bool value)
{
    enabled = value;
    for_each_child([value](Widget *child) { child->set_enabled(value); });
}


Widget::Widget(const Rect2D &rect)
    : rect(rect)
{
}


Widget::~Widget()
{
    if (parent)
    {
        parent->children.erase(std::find(parent->children.begin(), parent->children.end(), this));
    }

    for_each_child([](Widget *child) {
        child->parent = nullptr;
        delete child;
    });
}


Widget *Widget::add_child(Widget *widget)
{
    assert(widget != nullptr);
    assert(widget != this);
    assert(widget->parent != this);
    assert(std::find(children.begin(), children.end(), this) == children.end());

    if (widget->parent != nullptr)
    {
        widget->parent->children.erase(std::find(widget->parent->children.begin(),
                                                 widget->parent->children.end(),
                                                 widget->parent));
        widget->parent = nullptr;
    }

    widget->parent = this;
    children.push_back(widget);

    compose();

    return widget;
}


void Widget::dispatch_event(MouseButtonEvent &event)
{
    if (not enabled or not visible)
    {
        return;
    }

    Widget *widget = find_widget_at({static_cast<double>(event.x),
                                     static_cast<double>(event.y)},
                                    true,
                                    true);

    if (widget == nullptr)
    {
        mouse_button_event(event);
    }
    else
    {
        event.x -= (int) widget->rect.get_left_top().x;
        event.y -= (int) widget->rect.get_left_top().y;
        widget->dispatch_event(event);
    }
}


void Widget::dispatch_event(MouseMoveEvent &event)
{
    if (not enabled or not visible)
    {
        return;
    }

    Widget *widget = find_widget_at({static_cast<double>(event.x),
                                     static_cast<double>(event.y)},
                                    true,
                                    true);

    if (widget == nullptr)
    {
        widget = this;
    }

    Window *window = Application::get_instance().get_window();
    if (window == nullptr)
    {
        return;
    }

    if (window->get_widget_under_mouse() != widget)
    {
        window->set_widget_under_mouse(widget);
    }

    if (widget == this)
    {
        mouse_move_event(event);
    }
    else
    {
        event.x -= (int) widget->rect.get_left_top().x;
        event.y -= (int) widget->rect.get_left_top().y;
        widget->dispatch_event(event);
    }
}


void Widget::dispatch_event(KeyPressEvent &event)
{
    if (not enabled)
    {
        return;
    }

    Window *window = Application::get_instance().get_window();
    if (window == nullptr)
    {
        return;
    }

    Widget *focus = window->get_keyboard_focus();
    if (focus == nullptr)
    {
        return;
    }

    if (focus->is_visible() and focus->is_enabled())
    {
        focus->key_press_event(event);
    }
}


void Widget::dispatch_event(PaintEvent)
{
    if (not visible)
    {
        return;
    }

    paint_event({});
    for_each_child([](Widget *child) {
        if (child->visible)
        {
            child->dispatch_event(PaintEvent{});
        }
    });
}


void Widget::dispatch_event(TickEvent &event)
{
    if (not visible)
    {
        return;
    }

    tick_event(event);
    for_each_child([&event](Widget *child) { child->dispatch_event(event); });
}


Widget *Widget::find_widget_at(Point2D point, bool checkVisible, bool checkEnabled) const
{
    Widget *res = nullptr;

    for_each_child([point, &res, checkEnabled, checkVisible](Widget *child) {
        if (res != nullptr)
        {
            return;
        }

        if ((not checkVisible or child->visible)
            and (not checkEnabled or child->enabled)
            and collision::point_rect(point, child->rect))
        {
            res = child;
        }
    });

    return res;
}


void Widget::drop_absolute_position_cache()
{
    abs_pos = std::nullopt;
    for_each_child([](Widget *child) { child->drop_absolute_position_cache(); });
}


void Widget::compose()
{
    // Компоновщик вызывает функции setPosition и setSize блокировка необходима для избежания рекурсии
    if (compose_lock)
    {
        return;
    }

    compose_lock = true;
    RaiiAction lock = [this]() {
        compose_lock = false;
    };

    layout.compose(this);
}


Widget *Widget::get_parent() const
{
    return parent;
}

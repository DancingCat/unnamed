#include "Font.hpp"

#include <iostream>
#include <map>
#include <string_view>

#include "ui/FontCache.hpp"


Font::Font(std::string_view fontName, int size)
{
    handle = FontCache::get_instance().load(fontName, size);
}


TTF_Font *Font::get_handle() const
{
    return const_cast<TTF_Font *>(handle);
}

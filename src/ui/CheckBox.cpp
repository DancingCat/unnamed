#include "CheckBox.hpp"

#include "ui/Font.hpp"
#include "ui/Painter.hpp"
#include "ui/Style.hpp"


CheckBox::CheckBox(Point2D position, Vector2D size)
    : Widget(Rect2D{position, size})
    , text_texture{Texture::null()}
{
}


CheckBox::CheckBox(Vector2D size)
    : CheckBox(Point2D::null(), size)
{
}


void CheckBox::set_text(std::string text)
{
    this->text = std::move(text);
    text_texture.release();
}


bool CheckBox::is_checked() const
{
    return checked;
}


void CheckBox::set_checked(bool value)
{
    checked = value;
}


void CheckBox::mouse_button_event(const MouseButtonEvent &event)
{
    if (clicked and event.pressed)
    {
        set_checked(not is_checked());
        clicked(is_checked());
    }
}


void CheckBox::paint_event(PaintEvent)
{
    Painter painter(this);

    painter.set_color(Style<CheckBox>::background_color);
    painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));

    if (not is_enabled())
    {
        painter.set_color(Style<CheckBox>::disabled_color);
        painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));
    }
    else if (hover)
    {
        painter.set_color(Style<CheckBox>::hover_color);
        painter.fill_rect(Rect2D(Point2D(0, 0), get_size()));
    }

    double offset = Style<CheckBox>::button_offset * get_size().dy;
    double height = get_size().dy - offset * 2;

    if (hover && not checked)
    {
        painter.set_color(Style<CheckBox>::background_color);
        painter.fill_rect(Rect2D(Point2D(offset, offset), Vector2D(height, height)));
    }
    else if (checked)
    {
        painter.set_color(Style<CheckBox>::checked_color);
        painter.fill_rect(Rect2D(Point2D(offset, offset), Vector2D(height, height)));
    }

    painter.set_color(Style<CheckBox>::foreground_color);
    painter.draw_rect(Rect2D(Point2D(offset, offset), Vector2D(height, height)));

    if (text.empty())
    {
        return;
    }

    if (text_texture.is_null())
    {
        text_texture = Texture::render_text(
            Font{Style<CheckBox>::font_name, Style<CheckBox>::font_size},
            text,
            Style<CheckBox>::foreground_color);
    }

    Vector2D t_size = text_texture.get_size();
    Vector2D size = get_size() - Vector2D{height + offset * 2, 0};
    Point2D t_pos = {height + offset * 2, size.dy / 2 - t_size.dy / 2};

    Rect2D t_rect = {t_pos, t_size};
    painter.draw_texture(text_texture, t_rect);
}


void CheckBox::mouse_in_event(MouseInEvent)
{
    hover = true;
}


void CheckBox::mouse_out_event(MouseOutEvent)
{
    hover = false;
}

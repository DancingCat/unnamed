#pragma once

#include <map>
#include <string_view>

#include <SDL_ttf.h>


/// Информация о шрифте
struct FontInfo
{
    TTF_Font *font;        ///< Указатель на ресурс
    int size;              ///< Размер
    std::string_view name; ///< Название
};


/**
 * @brief Статическое хранилище шрифтов
 */
class FontCache
{
    std::map<std::pair<std::string_view, int>, TTF_Font *> data;
    static FontCache *instance;

public:
    /**
     * @brief Получить экземпляр класса
     */
    static FontCache &get_instance();

    /**
     * @brief Статическое хранилище шрифтов
     */
    FontCache();

    ~FontCache();

    /**
     * @brief Загрузить шрифт
     * @param fontName Название шрифта
     * @param size Размер
     * @return Ссылка на информацию о шрифте
     *
     * Поддерживает кеширование, при повторном обращении возвращает ссылку на созданный ранее объект.
     * Если при загрузке шрифта произойдет ошибка - она будет выведена на стандартный вывод, а в
     * структуре указатель на ресурс будет nullptr.
     */
    TTF_Font *load(std::string_view fontName, int size);

    FontCache(FontCache &) = delete;
    FontCache(FontCache &&) = delete;
    FontCache &operator=(FontCache &) = delete;
    FontCache &operator=(FontCache &&) = delete;
};

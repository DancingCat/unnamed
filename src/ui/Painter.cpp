#include "Painter.hpp"

#include <cassert>

#include <SDL.h>
#include <SDL_ttf.h>

#include "Application.hpp"
#include "ui/Texture.hpp"
#include "ui/Widget.hpp"
#include "ui/Window.hpp"


namespace
{
    SDL_FRect convert(const Rect2D &rect)
    {
        return {.x = static_cast<float>(rect.get_left_top().x),
                .y = static_cast<float>(rect.get_left_top().y),
                .w = static_cast<float>(rect.get_size().dx),
                .h = static_cast<float>(rect.get_size().dy)};
    }
} // namespace


Painter::Painter(Widget *widget)
    : renderer(nullptr)
    , offset(Vector2D{widget->get_absolute_position().x, widget->get_absolute_position().y})
    , target_size(widget->get_size())
{
    assert(widget != nullptr);

    Window *window = Application::get_instance().get_window();
    if (window == nullptr)
    {
        return;
    }

    renderer = window->get_renderer();

    // Отспуп в 1 чтобы не обрезало "лишнего"
    constexpr int offset_delta = 1;
    Rect2D widget_rect = widget->get_rect();
    SDL_Rect rect = {.x = (int) offset.dx - offset_delta,
                     .y = (int) offset.dy - offset_delta,
                     .w = (int) widget_rect.get_size().dx + offset_delta * 2,
                     .h = (int) widget_rect.get_size().dy + offset_delta * 2};
    SDL_RenderSetClipRect(renderer, &rect);
}


Vector2D Painter::get_target_size() const
{
    return target_size;
}


Painter::~Painter()
{
    SDL_RenderSetClipRect(renderer, nullptr);
}


void Painter::set_color(Color color) const
{
    SDL_SetRenderDrawColor(renderer, color.red, color.green, color.blue, color.alpha);
}


void Painter::draw_rect(Rect2D rect) const
{
    rect.set_left_top(rect.get_left_top() + offset);
    auto r = convert(rect);
    SDL_RenderDrawRectF(renderer, &r);
}


void Painter::fill_rect(Rect2D rect) const
{
    rect.set_left_top(rect.get_left_top() + offset);
    auto r = convert(rect);
    SDL_RenderFillRectF(renderer, &r);
}


void Painter::draw_line(Line2D line) const
{
    line.set_first_point(line.get_first_point() + offset);
    line.set_second_point(line.get_second_point() + offset);
    SDL_RenderDrawLineF(renderer,
                        (float) line.get_first_point().x,
                        (float) line.get_first_point().y,
                        (float) line.get_second_point().x,
                        (float) line.get_second_point().y);
}


void Painter::draw_texture(const Texture &texture, Rect2D rect)
{
    if (texture.is_null())
    {
        return;
    }

    rect.set_left_top(rect.get_left_top() + offset);
    SDL_FRect dst = convert(rect);
    SDL_RenderCopyF(renderer, texture.get_handle(), nullptr, &dst);
}


void Painter::clear() const
{
    SDL_RenderClear(renderer);
}


void Painter::present() const
{
    SDL_RenderPresent(renderer);
}


SDL_Renderer *Painter::get_handle()
{
    return renderer;
}

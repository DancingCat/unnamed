#pragma once

#include "math/Line2D.hpp"
#include "math/Rect2D.hpp"
#include "ui/Color.hpp"


struct SDL_Renderer;
class Texture;


/**
 * @brief Интерфейс для рисования
 */
class Painter
{
    SDL_Renderer *renderer;
    Vector2D offset;
    Vector2D target_size;

public:
    /**
     * @brief Интерфейс для рисования
     * @param renderer Рендерер. Зависит от реализации графики.
     */
    explicit Painter(class Widget *widget);

    ~Painter();

    /**
     * @brief Установить цвет для рисования
     */
    void set_color(Color color) const;

    /**
     * @brief Нарисовать прямоугольник (контур)
     */
    void draw_rect(Rect2D rect) const;

    /**
     * @brief Залить прямоугольник
     */
    void fill_rect(Rect2D rect) const;

    /**
     * @brief Нарисовать линию
     */
    void draw_line(Line2D line) const;

    /**
     * @brief Нарисовать текстуру
     * @param rect Прямоугольник (расположение и размер)
     * @param position Расположение (верхний левый угол)
     */
    void draw_texture(const Texture &texture, Rect2D rect);

    /**
     * @brief Очистить буфер (залить текущим цветом)
     */
    void clear() const;

    /**
     * @brief Скопировать содержимое буфера в целевой буфер (окно или текстура)
     */
    void present() const;

    /**
     * @brief Указатель на внутренний ресурс
     */
    SDL_Renderer *get_handle();

    /**
     * @brief Размер цели рендера
     */
    Vector2D get_target_size() const;

    Painter(Painter &) = delete;
    Painter(Painter &&) = delete;
    Painter &operator=(Painter &) = delete;
    Painter &operator=(Painter &&) = delete;
};

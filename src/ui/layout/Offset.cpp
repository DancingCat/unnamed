#include "Offset.hpp"


layout::Offset layout::Offset::recalculate_by_size(const Offset &offset, double size)
{
    if (offset.type == Absolute)
    {
        return offset;
    }

    return Offset{.value = offset.value * size, .type = Absolute};
}

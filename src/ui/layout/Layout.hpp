#pragma once

#include <memory>


class Widget;


/// Компоновщики
namespace layout
{
    /// Детали реализации
    namespace detail
    {
        struct AbstractLayout
        {
            virtual ~AbstractLayout() = default;

            virtual void compose(Widget *target) const = 0;
        };

        template <typename T>
        struct LayoutImpl;
    } // namespace detail


    /**
     * @brief Полиморфная обертка над любым компоновщиком
     *
     * Требует в конструктор объект любого класса с методом compose(Widget *) const,
     * возвращаемое значение (при наличии) игнорируется.
     *
     * При создании копирует поданный в конструктор объект и обеспечивает распределенное владение между всеми копиями
     * объекта Layout.
     */
    class Layout
    {
        std::shared_ptr<detail::AbstractLayout> impl;

    public:
        /**
         * @brief Полиморфная обертка над любым компоновщиком
         *
         * Конструктор по-умолчанию. Создает компоновщик, который ничего не делает (опция "не компоновать").
         */
        Layout() = default;

        /**
         * @brief Полиморфная обертка над любым компоновщиком
         * @param impl Объект любого класса с методом compose(Widget *) const
         *
         * Объект @c impl будет скопирован.
         */
        template <typename T>
        explicit Layout(T impl)
            : impl(std::make_shared<detail::LayoutImpl<T>>(std::move(impl)))
        {
            static_assert(
                requires(const T obj, Widget *widget) { obj.compose(widget); },
                "Объект должен содержать метод compose(Widget *) const");

            static_assert(std::is_nothrow_copy_constructible_v<T>, "Нет доступа к конструктору копирования");

            static_assert(std::is_nothrow_move_constructible_v<T>, "Нет доступа к конструктору перемещения");
        }

        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         */
        void compose(Widget *target) const
        {
            if (impl != nullptr)
            {
                impl->compose(target);
            }
        }
    };
} // namespace layout


template <typename T, typename U>
    requires requires(T t, U u, Widget *w) {
        t.compose(w);
        u.compose(w);
    }
layout::Layout operator and(T left, U right)
{
    struct Obj
    {
        T a;
        U b;

        void compose(Widget *w) const
        {
            a.compose(w);
            b.compose(w);
        }
    };

    return layout::Layout{
        Obj{left, right}
    };
}


/// Детали реализации
namespace layout::detail
{
    template <typename T>
    struct LayoutImpl : public AbstractLayout
    {
        std::decay_t<T> base;

        explicit LayoutImpl(T base)
            : base(base)
        {
        }

        void compose(Widget *target) const override
        {
            base.compose(target);
        }
    };
} // namespace layout::detail

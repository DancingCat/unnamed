#pragma once


/// Компоновщики
namespace layout
{
    /**
     * @brief Параметры отступа
     */
    struct Offset
    {
        /**
         * @brief Тип отступа
         */
        enum Type
        {
            Absolute, ///< Абсолютный (в пикселях)
            Relative  ///< Относительный (от 0 до 1). UB при значениях не в диапазоне от 0 до 1
        };

        double value = 0;     ///< Значение
        Type type = Absolute; ///< Тип

        /**
         * @brief Пересчитать относительный отступ в абсолютный по размеру
         *
         * @param offset Относительный отступ
         * @param size Размер для пересчета
         * @return Абсолютный отступ
         */
        static Offset recalculate_by_size(const Offset &offset, double size);

        /**
         * @brief Создать пустой объект (не делать отспуп)
         */
        static constexpr Offset no_offset()
        {
            return Offset{};
        }
    };
} // namespace layout

#pragma once

#include "ui/layout/Offset.hpp"


class Widget;


/// Компоновка без изменения размера
namespace layout::no_resize
{
    /**
     * @brief Вертикальная компоновка
     *
     * Распределяет виджеты на равном расстоянии по горизонтали. Не меняет горизонтальное расположение.
     */
    class Vertical
    {
        Offset offset_top;
        Offset offset_bottom;

    public:
        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         * @param offsetTop Отступ сверху
         * @param offsetBottom Отступ снизу
         */
        static void compose(Widget *widget, Offset offsetTop, Offset offsetBottom);

        /**
         * @brief Вертикальная компоновка
         * @param offsetTop Отступ сверху
         * @param offsetBottom Отступ снизу
         */
        explicit Vertical(Offset offsetTop, Offset offsetBottom);

        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         */
        void compose(Widget *target) const;
    };
} // namespace layout::no_resize

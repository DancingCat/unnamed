#include "Vertical.hpp"

#include "ui/Widget.hpp"

#include <cassert>


void layout::no_resize::Vertical::compose(Widget *widget, Offset offsetTop, Offset offsetBottom)
{
    assert(widget != nullptr);

    double width = widget->get_size().dx;

    if (offsetTop.type == Offset::Relative)
    {
        offsetTop = Offset::recalculate_by_size(offsetTop, width);
    }

    if (offsetBottom.type == Offset::Relative)
    {
        offsetBottom = Offset::recalculate_by_size(offsetBottom, width);
    }

    double children_size = 0;
    int child_count = 0;

    widget->for_each_child([&children_size, &child_count](Widget *widget) {
        children_size += widget->get_size().dy;
        ++child_count;
    });

    if (child_count == 0)
    {
        return;
    }

    double delta = (widget->get_size().dy - children_size - offsetTop.value - offsetTop.value) / (double) (child_count - 1);
    delta = delta < 0 ? 0 : delta;

    double next_pos = offsetTop.value;

    widget->for_each_child([&next_pos, delta](Widget *child) {
        child->set_position(Point2D{child->get_position().x, next_pos});
        next_pos += delta + child->get_size().dy;
    });
}


layout::no_resize::Vertical::Vertical(Offset offsetTop, Offset offsetBottom)
    : offset_top(offsetTop)
    , offset_bottom(offsetBottom)
{
}


void layout::no_resize::Vertical::compose(Widget *target) const
{
    compose(target, offset_top, offset_bottom);
}

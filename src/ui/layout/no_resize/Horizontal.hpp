#pragma once

#include "ui/layout/Offset.hpp"


class Widget;


/// Компоновка без изменения размера
namespace layout::no_resize
{
    /**
     * @brief Горизонтальная компоновка
     *
     * Распределяет виджеты на равном расстоянии по горизонтали. Не меняет вертикальное расположение.
     */
    class Horizontal
    {
        Offset offset_left;
        Offset offset_right;

    public:
        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         * @param offsetLeft Отступ слева
         * @param offsetRight Отступ справа
         */
        static void compose(Widget *widget, Offset offsetLeft, Offset offsetRight);

        /**
         * @brief Горизонтальная компоновка
         * @param offsetLeft Отступ слева
         * @param offsetRight Отступ справа
         */
        explicit Horizontal(Offset offsetLeft, Offset offsetRight);

        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         */
        void compose(Widget *target) const;
    };
} // namespace layout::no_resize

#include "Horizontal.hpp"

#include "ui/Widget.hpp"

#include <cassert>


void layout::no_resize::Horizontal::compose(Widget *widget, Offset offsetLeft, Offset offsetRight)
{
    assert(widget != nullptr);

    double width = widget->get_size().dx;

    if (offsetLeft.type == Offset::Relative)
    {
        offsetLeft = Offset::recalculate_by_size(offsetLeft, width);
    }

    if (offsetRight.type == Offset::Relative)
    {
        offsetRight = Offset::recalculate_by_size(offsetRight, width);
    }

    double children_size = 0;
    int child_count = 0;

    widget->for_each_child([&children_size, &child_count](Widget *widget) {
        children_size += widget->get_size().dx;
        ++child_count;
    });

    if (child_count == 0)
    {
        return;
    }

    double delta = (widget->get_size().dx - children_size - offsetLeft.value - offsetRight.value) / (double) (child_count - 1);
    delta = delta < 0 ? 0 : delta;

    double next_pos = offsetLeft.value;

    widget->for_each_child([&next_pos, delta](Widget *child) {
        child->set_position(Point2D{next_pos, child->get_position().y});
        next_pos += delta + child->get_size().dx;
    });
}


layout::no_resize::Horizontal::Horizontal(Offset offsetLeft, Offset offsetRight)
    : offset_left(offsetLeft)
    , offset_right(offsetRight)
{
}


void layout::no_resize::Horizontal::compose(Widget *target) const
{
    compose(target, offset_left, offset_right);
}

#pragma once

#include "ui/layout/Offset.hpp"


class Widget;


/// Компоновка без изменения размера
namespace layout::no_resize
{
    /**
     * @brief Компоновка с привязкой к определенной стороне
     *
     * Не изменяет положение виджетов по ортогональной оси.
     */
    class Anchor
    {
    public:
        /// Сторона привязки
        enum Side
        {
            Left,  ///< Слева
            Right, ///< Справа
            Top,   ///< Сверху
            Bottom ///< Снизу
        };

    private:
        Side side;
        Offset offset;

    public:
        /**
         * @brief Компоновка с привязкой к определенной стороне
         * @param side Сторона привязки
         * @param offset Отступ
         */
        Anchor(Side side, Offset offset);

        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         */
        void compose(Widget *target) const;

        /**
         * @brief Скомпоновать
         * @param widget Виджет для компоновки
         * @param side Сторона привязки
         * @param offset Отступ
         */
        static void compose(Widget *target, Side side, Offset offset);
    };
} // namespace layout::no_resize

#include "Anchor.hpp"

#include "ui/Widget.hpp"

#include <cassert>


layout::no_resize::Anchor::Anchor(Side side, Offset offset)
    : side(side)
    , offset(offset)
{
}


void layout::no_resize::Anchor::compose(Widget *target) const
{
    compose(target, side, offset);
}


void layout::no_resize::Anchor::compose(Widget *target, Side side, Offset offset)
{
    assert(target != nullptr);

    switch (side)
    {
    case Left:
        target->for_each_child(
            [off = Offset::recalculate_by_size(offset, target->get_size().dx).value](Widget *widget) {
                widget->set_position(Point2D{off, widget->get_position().y});
            });
        break;

    case Right:
        target->for_each_child(
            [off = target->get_size().dx - Offset::recalculate_by_size(offset, target->get_size().dx).value](Widget *widget) {
                widget->set_position(Point2D{off - widget->get_size().dx, widget->get_position().y});
            });
        break;

    case Top:
        target->for_each_child(
            [off = Offset::recalculate_by_size(offset, target->get_size().dy).value](Widget *widget) {
                widget->set_position(Point2D{widget->get_position().x, off});
            });
        break;

    case Bottom:
        target->for_each_child(
            [off = target->get_size().dy - Offset::recalculate_by_size(offset, target->get_size().dy).value](Widget *widget) {
                widget->set_position(Point2D{widget->get_position().x, off - widget->get_size().dy});
            });
        break;
    }
}

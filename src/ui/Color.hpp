#pragma once

#include <cstdint>


/**
 * @brief Цвет
 */
struct Color
{
    uint8_t red;          ///< Красный
    uint8_t green;        ///< Зеленый
    uint8_t blue;         ///< Синий
    uint8_t alpha = 0xFF; ///< Прозрачность
};

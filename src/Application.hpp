#pragma once


class Window;


/**
 * @brief Класс приложения
 */
class Application
{
    static Application *instance;

    Window *window_widget;

    int code = 0;
    bool run = false;

public:
    /**
     * @brief Экземпляр класса
     */
    static Application &get_instance();

    /**
     * @brief Класс приложения
     *
     * @note Не рассчитан на создание второго экземпляра
     */
    Application();

    ~Application();

    /**
     * @brief Запуск событийного цикла
     * @return Код возврата (0 - нет ошибок)
     */
    int execute();

    /**
     * @brief Завершить событийный цикл
     * @param exitCode Код завершения
     */
    void quit(int exitCode = 0);

    /**
     * @brief Получить окно приложения
     */
    inline Window *get_window()
    {
        return window_widget;
    }

    Application(Application &) = delete;
    Application(Application &&) = delete;
    Application &operator=(Application &) = delete;
    Application &operator=(Application &&) = delete;
};
